//
// link_performance.cc
// (c) Yongjin Park, Apr 2, 2012
//
#include "performance.hh"
#include <boost/lexical_cast.hpp>

enum curve_type { prc_t, roc_t };
const size_t ninter_default = 1000;
const double minval_default = 0;
const double maxval_default = 1;

void print_help(){
    cerr << "<usage>\nperformance type summary .link test.pairs [ninter] [minval] [maxval]\n" << endl;
    cerr << "  type      : " << prc_t << "=precision recall; ";
    cerr << roc_t << "=receiver operating characteristic (ROC)" << endl;
    cerr << "  summary   : 0=output curve; 1=output summary stat (area under curve)" << endl;
    cerr << "  .link     : link prediction file with" << endl;
    cerr << "              u <\\t> v <\\t> score <\\n> per line" << endl;
    cerr << "  test.pairs: test data with" << endl;
    cerr << "              u <\\t> v <\\t> {0,1} <\\n> per line" << endl;;
    cerr << "  ninter    : number of intervals (default=" << ninter_default << ")" << endl;
    cerr << "              to partition [nmin, nmax] of score values" << endl;
    cerr << "  minval    : minimum value of score (default=" << minval_default << ")" << endl;
    cerr << "  maxval    : maximum value of score (default=" << maxval_default << ")" << endl;
    cerr << endl;
}
// measure link prediction performance
// performance type summary .link test.pairs
int main(int argc, const char* argv[]){
    if( argc < 5 ){ print_help(); return -1; }
    int type = lexical_cast<int>(argv[1]);
    bool summary = lexical_cast<int>(argv[2]) > 0;
    size_t ninter = ninter_default;
    double minval = minval_default;
    double maxval = maxval_default;

    boost::shared_ptr< vector<link_ptr_t> > link_list_ptr =
            read_link_list( argv[3], argv[4] );


    if( argc > 5 ){ ninter = lexical_cast<size_t>(argv[5]); }
    if( argc > 6 ){ minval = lexical_cast<double>(argv[6]); }
    if( argc > 7 ){ maxval = lexical_cast<double>(argv[7]); }


    boost::shared_ptr< vector<double> > xvec_ptr;
    boost::shared_ptr< vector<double> > yvec_ptr;

    if( type == roc_t ){
        tie(xvec_ptr, yvec_ptr) =
                make_regular_roc_curve( link_list_ptr, ninter, minval, maxval );
    }else if( type == prc_t ){
        tie(xvec_ptr, yvec_ptr) =
                make_regular_pr_curve( link_list_ptr, ninter, minval, maxval );
    }else{
        cerr << "unknown score type: " << type << endl;
        return -1;
    }

    if( summary ){
        if( type == roc_t ){ xvec_ptr->push_back(1); yvec_ptr->push_back(1); }
        if( type == prc_t ){ xvec_ptr->push_back(1); yvec_ptr->push_back(0); }
        cout << auc(xvec_ptr, yvec_ptr) << endl;
    }else{
        vector<double>& xvec = *(xvec_ptr.get());
        vector<double>& yvec = *(yvec_ptr.get());
        assert( xvec.size() == yvec.size() );
        for(int pos=0; pos<xvec.size(); ++pos){
            cout << xvec[pos] << "\t" << yvec[pos] << endl;
        }
    }

    return 1;
}
