/////////////////////////////////////////////////////
//
// Dynamic Hierarchical Agglomerative Clustering
//
// (c) 2010-2012, Yongjin Park (ypark28@jhu.edu)
//                Joel S. Bader (joel.bader@jhu.edu)
//
//     Bader Lab, Johns Hopkins University
//
// Distributed under BOOST Software license
// see "boost_1_49_0/LICENSE_1_0.txt"
//
// Source code   : Yongjin Park
// Contributors  : Yongjin Park, Joel Bader
//
/////////////////////////////////////////////////////

#include "util.hh"
// boost multi array
template<typename T>
void init_vec(vector<T>& vec, size_t K){
    for(size_t k=0; k<K; ++k) vec[k] = 0;
}

template<typename T>
void init_mat(multi_array<T,2>& mat, size_t nRows, size_t nCols){
    for(size_t r=0; r<nRows; ++r)
        for(size_t c=0; c<nCols; ++c)
            mat[r][c] = 0;
}

////////////////////////////////////////////////////////////////
// Simple math
// safe logarithm
inline
double safe_log(double val){
    if( val <= 0. ) return 0.;
    return log(val);
}

// output log-likelihood trace
void write_llik_vec( const char* o_hdr, vector<double>* llik_vec ){
    ofstream llik_out( (lexical_cast<string>(o_hdr)+"llik.txt").c_str() );
    for_each( llik_vec->begin(), llik_vec->end(), func_llik_out(llik_out) );
    llik_out.close();
}

////////////////////////////////////////////////////////////////
// Get Time String
//
void _getTimeStr( char* dstr, char* tstr, time_t tt ) {
    struct tm* timeinfo;
    timeinfo = localtime( &tt );
    strftime( dstr, 20, "%x", timeinfo );
    strftime( tstr, 20, " %X", timeinfo );
}

string get_curr_time() {
    time_t tt;
    time( &tt );
    char dstr[20];
    char tstr[20];
    _getTimeStr( dstr, tstr, tt );
    return (string( dstr ) + " " + string( tstr ));
}

// Measure time interval
// - basically same as Matlab's tic&toc
// Note: not thread-safe
void tic(){ time(&_Time_Tic); }
int toc(){
    time_t _Time_Toc;
    time(&_Time_Toc);
    return difftime(_Time_Toc,_Time_Tic);
}



////////////////////////////////////////////////////////////////
// string
string get_fname( string& file_str ){

    int spos = file_str.rfind('/');
    int epos = file_str.rfind('.');

    if( spos==string::npos ) spos = -1;
    if( epos==string::npos ) epos = file_str.size();

    return file_str.substr(spos+1,(epos-1)-spos);
}

string get_ext( string& file_str ){
    size_t len = file_str.size();
    size_t epos = file_str.rfind('.');
    if( epos == string::npos || epos == len-1 )
        return string("");
    return file_str.substr(epos+1,len-(epos+1));
}
// EOF
