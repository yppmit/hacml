/////////////////////////////////////////////////////
//
// Dynamic Hierarchical Agglomerative Clustering
//
// (c) 2010-2012, Yongjin Park (ypark28@jhu.edu)
//                Joel S. Bader (joel.bader@jhu.edu)
//
//     Bader Lab, Johns Hopkins University
//
// Distributed under BOOST Software license
// see "boost_1_49_0/LICENSE_1_0.txt"
//
// Source code   : Yongjin Park
// Contributors  : Yongjin Park, Joel Bader
//
/////////////////////////////////////////////////////

#include "dhac.hh"


// ==========================================================
void print_help(){
    cerr << "\n<usage>" << endl;
    cerr << "dhac b s data1.pairs data2.pairs ..." << endl;
    cerr << " b      = bandwidth (>0)" << endl;
    cerr << " s      = scope (1: immediate, 2: neighbors of neighbors)" << endl;
    cerr << " .pairs = temporally ordered data files" << endl;
    cerr << "          where each line contains" << endl;
    cerr << "            u <\\t> v <\\n>" << endl;
    cerr << "          with integer-valued nodes" << endl;
    cerr << "          file names must be distinguishable" << endl;
    cerr << endl;
    cerr << "DHAC (c) 2011-2012 Yongjin Park (ypark28@jhu.edu)" << endl;
}

// ==========================================================
int main(int argc, const char* argv[]){

    if(argc < 4){ print_help(); return -1; }

    cerr << "Start: DHAC-local" << endl;

    double bandwidth = atof(argv[1]);
    int scope = atoi(argv[2]);
    bool approx = (scope == 1) ? true : false;
    int Tmax = argc - 3; // argv[3], argv[4], ...

    double tot_max_score = 0.;

    for(int t=0; t<Tmax; ++t){
        int curr_arg_pos = t+3;
        hac_data_ptr_vec_t data_vec;
        vector<double> kern_weights;
        kernel_t kern(t,Tmax,bandwidth);

        // read relevant data points
        // current snapshot and weight
        data_vec.push_back( read_upair_file<hac_data_network_t>(argv[curr_arg_pos]) );
        kern_weights.push_back( kern.val(t) );
        for(int s=0; s<Tmax; ++s){
            if( s==t ) continue;
            double w = kern.val(s);
            if( w < kern_cutoff ){ continue; }
            data_vec.push_back( read_upair_file<hac_data_network_t>(argv[3+s]) );
            kern_weights.push_back(w);
        }

        // 1st pass
        score_vec_ptr_t score_vec_ptr;
        merge_pair_vec_ptr_t merge_pair_vec_ptr;

        tie(score_vec_ptr, merge_pair_vec_ptr) =
                run_hac_global(data_vec, kern_weights, approx);

        // shared_ptr will be cleared out of memory
        data_vec.clear(); kern_weights.clear();

        // find maximum and argmax K
        score_vec_t& score_vec = *score_vec_ptr.get();
        int tot_merges = score_vec.size();
        cerr << "Total #merges = " << tot_merges << endl;
        int argmax = 0;
        double max_score = 0;
        for(int pos=1; pos<tot_merges; ++pos){
            if( score_vec[pos] > max_score )
            { argmax=pos; max_score=score_vec[pos]; }
        }
        cerr << "Best found at #merges = " << argmax << endl;
        tot_max_score += max_score;

        string data_file(argv[curr_arg_pos]);

#ifdef DEBUG
        // 2nd pass
        // read relevant data points
        // current snapshot and weight
        data_vec.push_back( read_upair_file<hac_data_network_t>(argv[curr_arg_pos]) );
        kern_weights.push_back( kern.val(t) );

        for(int s=0; s<Tmax; ++s){
            if( s==t ) continue;
            double w = kern.val(s);
            if( w < kern_cutoff ){ continue; }
            data_vec.push_back( read_upair_file<hac_data_network_t>(argv[3+s]) );
            kern_weights.push_back(w);
        }

        run_hac_global_2nd( data_vec, kern_weights, merge_pair_vec_ptr, argmax );

        // output
        write_group_info( data_vec[0], 
                          (get_fname(data_file) + "-debug.group").c_str() );
#endif
        // group structure of current snapshot
        hac_data_ptr_t Gptr = read_upair_file<hac_data_network_t>( argv[curr_arg_pos] );
        write_group_info<hac_data_network_t>(*(merge_pair_vec_ptr.get()), 
                                             *(Gptr.get()), argmax,
                                             (get_fname(data_file) + ".group").c_str());
    }

    cerr << "Done: DHAC" << endl;

    return 1;
}
// EOF
