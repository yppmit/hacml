/////////////////////////////////////////////////////
//
// Dynamic Hierarchical Agglomerative Clustering
//
// (c) 2010-2012, Yongjin Park (ypark28@jhu.edu)
//                Joel S. Bader (joel.bader@jhu.edu)
//
//     Bader Lab, Johns Hopkins University
//
// Distributed under BOOST Software license
// see "boost_1_49_0/LICENSE_1_0.txt"
//
// Source code   : Yongjin Park
// Contributors  : Yongjin Park, Joel Bader
//
/////////////////////////////////////////////////////

#ifndef DHAC_IO_HH
#define DHAC_IO_HH

#include<iostream>
#include<fstream>
#include"ds.hh"
#include"alg.hh"
#include<vector>
using namespace std;
using namespace boost;

// ================================================================
// simple I/O functions

template<typename hd_type, bool weighted> 
boost::shared_ptr<hd_type> read_pair_file( const char* );

template<typename hd_type, bool weighted>
void write_pair_file( boost::shared_ptr<hd_type> data_ptr, const char* outfile );

template<typename hd_type>
boost::shared_ptr<hd_type> read_upair_file( const char* infile );

template<typename hd_type>
boost::shared_ptr<hd_type> read_wpair_file( const char* infile );

template<typename hd_type>
void write_upair_file( boost::shared_ptr<hd_type> data_ptr, const char* outfile );

template<typename hd_type>
void write_wpair_file( boost::shared_ptr<hd_type> data_ptr, const char* outfile );


#ifdef DEBUG
void write_group_info( hac_data_ptr_t, const char* );
#endif

////////////////////////////////////////////////////////////////
template<typename hd_type>
boost::shared_ptr<hd_type> read_upair_file( const char* infile ){
    return read_pair_file<hd_type,false>(infile);
}

template<typename hd_type>
void write_upair_file( boost::shared_ptr<hd_type> data_ptr, const char* outfile ){
    write_pair_file<hd_type,false>(data_ptr, outfile);
}

template<typename hd_type>
boost::shared_ptr<hd_type> read_wpair_file( const char* infile ){
    return read_pair_file<hd_type,true>(infile);
}

template<typename hd_type>
void write_wpair_file( boost::shared_ptr<hd_type> data_ptr, const char* outfile ){
    write_pair_file<hd_type,true>(data_ptr, outfile);
}


template<typename hd_type, bool weighted> 
boost::shared_ptr<hd_type> read_pair_file( const char* infile ) {

    boost::shared_ptr<hd_type> out( new hd_type() );

    hd_type& G = *(out.get());

    ifstream fin( infile, ios::in );
    int u, v;
    if(weighted){
        double w;
        while (fin >> u >> v >> w) {
            if(!G.has_node(u)) G.add_node(u,1.);
            if(!G.has_node(v)) G.add_node(v,1.);
        }
    }else{
        while (fin >> u >> v) {
            if(!G.has_node(u)) G.add_node(u,1.);
            if(!G.has_node(v)) G.add_node(v,1.);
        }
    }
    fin.close();

    ifstream fin2( infile, ios::in );
    if(weighted){
        double w;
        while (fin2 >> u >> v >> w) {
            if (u == v) { continue; }
            if( !G.has_edge(u,v) ) G.add_data_edge(u,v,w);
        }
    }else{
        while (fin2 >> u >> v) {
            if (u == v) { continue; }
            if( !G.has_edge(u,v) ) G.add_data_edge(u,v,1.);
        }
    }
    fin2.close();

#ifdef DEBUG
    cerr << "+  I/O read edge file: " << infile;
    cerr << " V=" << G.get_tot_node_data();
    cerr << " E=" << G.get_tot_edge_data() << endl;
#endif

    return out;
}

template<typename hd_type>
struct func_write_pair {
    typedef typename hd_type::node_t node_t;
    func_write_pair(ofstream& _ofs): ofs(_ofs) {}
    void operator () (node_t u, node_t v){
        ofs << u << "\t" << v << endl;
    }    
    ofstream& ofs;
};

template<typename hd_type>
struct func_write_wpair {
    typedef typename hd_type::node_t node_t;

    func_write_wpair(hd_type& _hd, ofstream& _ofs): 
        hd(_hd), ofs(_ofs) {}

    void operator () (node_t u, node_t v){
        ofs << u << "\t" << v << "\t";
        ofs << hd.get_edge_data(u,v) << endl;
    }
    hd_type& hd;
    ofstream& ofs;
};


template<typename hd_type, bool weighted>
void write_pair_file( boost::shared_ptr<hd_type> data_ptr, const char* outfile ){

    hd_type& G = *(data_ptr.get());;
    hac_traits::pair_visitor_t func;

    ofstream fout( outfile, ios::out );

    if( weighted ){ func = func_write_wpair<hd_type>(G,fout); }
    else { func = func_write_pair<hd_type>(fout); }
    visit_immediate_pairs( G, func );
    fout.close();
}


// add feature file to network G
// append one feature either 0 or 1
// if vffile includes v in G
struct func_add_zero_feat {
    typedef hac_traits::node_t node_t;

    func_add_zero_feat(vf_hac_data_network_t& _g) : G(_g) {}

    void operator () (node_t u){
        G.get_feature_vec(u).push_back(0);
    }
    vf_hac_data_network_t& G;
};

void add_vertex_feature_file( vf_hac_data_network_t& G, const char* vffile ){

    hac_traits::node_visitor_t func = func_add_zero_feat(G);
    visit_nodes(G,func);

    typedef hac_traits::node_t node_t; 
    node_t u;
    ifstream ifs( vffile );
    while( ifs >> u ){
        if( G.has_node(u) ){
            vf_hac_data_network_t::feature_vec_t &vec
                = G.get_feature_vec(u);
#ifdef DEBUG
            assert( vec.size() > 0 );
#endif
            vec[ vec.size()-1 ] = 1;
        }
    }
    ifs.close();
}

#ifdef DEBUG
struct func_feat_dump {
    func_feat_dump( vf_hac_data_network_t& _g ): G(_g){ }
    void operator() ( vf_hac_data_network_t::node_t u ){
        vf_hac_data_network_t::feature_vec_t &vec
            = G.get_feature_vec(u);
        cerr << "node=" << u;
        for(size_t t=0; t<vec.size(); ++t)
            cerr << " " << vec[t];
        cerr << endl;
    }
    vf_hac_data_network_t& G;
};
// check current status of featues
void dump_feature_mat( vf_hac_data_network_t& G ){
    hac_traits::node_visitor_t ff = func_feat_dump( G );
    visit_nodes( G, ff );
}
#endif

typedef pair<hac_traits::node_t,hac_traits::node_t> node_pair_t;
typedef boost::shared_ptr<node_pair_t> node_pair_ptr_t;
typedef vector<node_pair_ptr_t> node_pair_vec_t;

typedef vector<hac_traits::node_t> children_vec_t;
typedef boost::unordered_map<hac_traits::node_t, children_vec_t> children_map_t;

// resolve group structure and output
// until reaching < stop_pt
void build_children_map( children_map_t& cmap, node_pair_vec_t& npvec, size_t stop_pt ){
    hac_traits::node_t u,v;

    for(size_t r=0; r<npvec.size(); ++r){
        tie(u,v) = *(npvec[r].get());
        if( cmap.count(u) == 0 ) cmap[u].push_back(u); 
        if( cmap.count(v) == 0 ) cmap[v].push_back(v);
    }

    // transfer things under right to left
    for(size_t r=0; r<stop_pt; ++r){
        tie(u,v) = *(npvec[r].get());
        children_vec_t& left = cmap[u];
        children_vec_t& right = cmap[v];

        for(size_t j=0; j<right.size(); ++j)
            left.push_back( right[j] );
        cmap.erase( v );
    }
}


void write_group_info( node_pair_vec_t& npvec, size_t stop_pt, const char* outfile ){

    assert( stop_pt <= npvec.size() );

    children_map_t cmap;
    hac_traits::node_t u;

    build_children_map( cmap, npvec, stop_pt );

    ofstream out( outfile, ios::out );
    for(children_map_t::const_iterator it=cmap.begin();
        it!=cmap.end(); ++it){
        u = it->first;
        if( cmap[u].size() < 1 ) continue;
        out << cmap[u][0];
        for(size_t j=1; j<cmap[u].size(); ++j)
            out << "\t" << cmap[u][j];
        out << endl;
    }
    out.close();
}

template<typename hd_type>
void write_group_info( node_pair_vec_t& npvec, hd_type& G, size_t stop_pt, const char* outfile ){

    assert( stop_pt <= npvec.size() );

    children_map_t cmap;
    hac_traits::node_t u,v;

    build_children_map( cmap, npvec, stop_pt );

    ofstream out( outfile, ios::out );
    for(children_map_t::const_iterator it=cmap.begin();
        it!=cmap.end(); ++it){
        u = it->first;
        if( cmap[u].size() < 1 ) continue;
        if( G.has_node(cmap[u][0]) ) out << cmap[u][0];
        for(size_t j=1; j<cmap[u].size(); ++j)
            if( G.has_node(cmap[u][j]) ) out << "\t" << cmap[u][j];
        out << endl;
    }
    out.close();
}


////////////////////////////////////////////////////////////////
#ifdef DEBUG
struct func_group_out{

    hac_data_network_t& hd;
    ofstream& ofs;

    func_group_out( hac_data_network_t& _hd, ofstream& _ofs ): hd(_hd), ofs(_ofs) { }

    typedef hac_traits::node_t node_t;

    void operator()( node_t u ){
        hac_traits::node_vec_t& children = *(hd.get_children(u));
        if( children.size() < 1 ) return;
        ofs << children[0];
        for(size_t u=1; u<children.size(); ++u){
            ofs << "\t" << children[u];
        }
        ofs << endl;
    }
};


void write_group_info( hac_data_ptr_t Gptr, const char* outfile ){

    hac_data_network_t& hd = *(Gptr.get());
    ofstream out(outfile, ios::out);

    hac_traits::node_visitor_t ff = func_group_out( hd, out );
    visit_nodes( hd, ff );
    out.close();
}
#endif // end of DEBUG

#endif
