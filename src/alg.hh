/////////////////////////////////////////////////////
//
// Dynamic Hierarchical Agglomerative Clustering
//
// (c) 2010-2012, Yongjin Park (ypark28@jhu.edu)
//                Joel S. Bader (joel.bader@jhu.edu)
//
//     Bader Lab, Johns Hopkins University
//
// Distributed under BOOST Software license
// see "boost_1_49_0/LICENSE_1_0.txt"
//
// Source code   : Yongjin Park
// Contributors  : Yongjin Park, Joel Bader
//
/////////////////////////////////////////////////////

#ifndef HAC_NETWORK_ALG_HH 
#define HAC_NETWORK_ALG_HH 

#include "ds.hh"
#include <vector>

const double default_data = 1.;
const double null_data = 0.;

struct hac_traits { 

    typedef hac_data_network_t::data_graph_t graph_t;
    typedef hac_data_network_t::node_t node_t;
    typedef hac_data_network_t::node_hash_t node_hash_t;
#ifdef DEBUG
    typedef hac_data_network_t::children_vec_t node_vec_t;
    typedef hac_data_network_t::edge_visitor_t edge_visitor_t;
#endif
    typedef hac_data_network_t::adj_data_visitor_t adj_data_visitor_t;
    typedef hac_data_network_t::union_neigh_data_visitor_t union_neigh_visitor_t;
    typedef hac_data_network_t::pair_visitor_t pair_visitor_t;
    typedef hac_data_network_t::pair_visitor_double_t pair_visitor_double_t;
    typedef hac_data_network_t::node_visitor_t node_visitor_t;

};


// O(V)
void visit_nodes(
    hac_data_network_t& hac_data,
     hac_traits::node_visitor_t& func );

// O(d)
void visit_immediate_pairs_single( 
    hac_data_network_t& hac_data,
     hac_traits::node_t u,
     hac_traits::pair_visitor_t& func );

// O(V) O(d)
void visit_immediate_pairs( hac_data_network_t& hac_data,
                            hac_traits::pair_visitor_t& func );

// O(d) O(d)
void visit_2ndorder_pairs_single( 
    hac_data_network_t& hac_data,
     hac_traits::node_t u,
     hac_traits::pair_visitor_t& func );

// O(V) O(d) O(d)
void visit_2ndorder_pairs( hac_data_network_t& hac_data,
                            hac_traits::pair_visitor_t& func );

// union neighbors for all (x,y) in  (N(u) + N(v)) x (N(u) + N(v))
//                         x < y
void
visit_union_neigh_pairs( hac_data_network_t& hac_data,
                         hac_traits::node_t u,
                         hac_traits::node_t v,
                         hac_traits::pair_visitor_t& func );

// union neighbors for all x in  N(u) + N(v)
// usually to compute some score using data
void 
visit_data_union_neighbors( hac_data_network_t& hac_data,
                             hac_traits::node_t u,
                             hac_traits::node_t v,
                             hac_traits::union_neigh_visitor_t&
                            func );


// visit other neighbors not in union neighbors of u and v
void 
visit_data_inv_union_neighbors( 
    hac_data_network_t& hac_data,
     hac_traits::node_t u,
     hac_traits::node_t v,
     hac_traits::union_neigh_visitor_t& func );


#ifdef DEBUG
// not quite useful... 
// also for debugging
void visit_data_edges( hac_data_network_t& hac_data,
                       hac_traits::edge_visitor_t& func );

// mainly for debugging purpose
// sum node data of a set
double
sum_nodes_within( hac_data_network_t::children_vec_t& set_u,
                  hac_data_network_t& other_hac_data );

// mainly for debugging purpose
// sum edge data between two sets on (perhaps other?) data
double
sum_edges_between( hac_data_network_t::children_vec_t& set_u,
                   hac_data_network_t::children_vec_t& set_v,
                   hac_data_network_t& other_hac_data );
#endif

#endif
// EOF
