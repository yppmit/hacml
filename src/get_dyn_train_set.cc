/////////////////////////////////////////////////////
//
// Dynamic Hierarchical Agglomerative Clustering
//
// (c) 2010-2011, Yongjin Park (ypark28@jhu.edu)
//                Joel S. Bader (joel.bader@jhu.edu)
//
//     Bader Lab, Johns Hopkins University
//
// Distributed under BOOST Software license
// see "boost_1_49_0/LICENSE_1_0.txt"
//
// Source code   : Yongjin Park
// Contributors  : Yongjin Park, Joel Bader
//
/////////////////////////////////////////////////////

#include "sample.hh"

// Add weighted edges in Gtest
// transfer true labeling of Gtot to Gtest
struct func_mk_test_set{
    func_mk_test_set( hac_data_network_t& gtot, hac_data_network_t& gtest ):
        Gtot(gtot), Gtest(gtest) {}

    void operator()( test_pair_ptr_t uv_pair ){
        int u = uv_pair->first, v = uv_pair->second;
        double lbl = Gtot.has_edge(u,v)? 
            Gtot.get_edge_data(u,v) : 0.;          // true label
        if(!Gtest.has_node(u)) Gtest.add_node(u,1.);//
        if(!Gtest.has_node(v)) Gtest.add_node(v,1.);//
        Gtest.add_data_edge(u,v,lbl);               // test data
    }

    hac_data_network_t& Gtot;
    hac_data_network_t& Gtest;
};

// remove edges from training set that are in the test set
// looping through test data set or template
struct func_mk_train_set{
    func_mk_train_set( hac_data_network_t& gtrain ):
        Gtrain(gtrain) {}

    // Remove edges in Gtrain
    void operator()( test_pair_ptr_t uv_pair ){
        int u = uv_pair->first, v = uv_pair->second;
        if( Gtrain.has_edge(u,v) ){ Gtrain.del_edge(u,v); }
    }

    hac_data_network_t& Gtrain;
};

struct func_mk_union{
    func_mk_union( hac_data_network_t& _uni ) : Guni(_uni) {}
    typedef hac_traits::node_t node_t;
    void operator()( node_t u, node_t v ){
        if(!Guni.has_node(u)) Guni.add_node(u,1);
        if(!Guni.has_node(v)) Guni.add_node(v,1);
        Guni.add_data_edge(u,v,1);
    }
    hac_data_network_t& Guni;
};

int main(int argc, const char* argv[]){

    if(argc < 5){
        cout << "get_dyn_train_set Pmiss corr seed G1.pairs G2.pairs ..." << endl;
        cout << endl;
        cout << " Pmiss : prob of missing links " << endl;
        cout << " corr  : time-correlation, 0 or 1" << endl;
        cout << " seed  : random seed" << endl;
        cout << " .pairs: data pairs files" << endl;
        cout << endl;
        return -1;
    }

    double Pmiss = lexical_cast<double>(argv[1]);
    int corr = lexical_cast<int>(argv[2]);
    int seed = lexical_cast<int>(argv[3]);
    if( Pmiss <= 0 || Pmiss >= 1. ){
        cout << "Pmiss should be in (0,1)" << endl; return -1;
    }

    int Tmax = argc-4;

    // time-"un"correlated, treat each snapshot independently
    if( corr != 1 ){
        for(int t = 0; t<Tmax; ++t){
            int arg_pos = t+4;
            string net_file( argv[arg_pos] );
            make_test_set( net_file, Pmiss, seed );
        }
        return 1;
    }

    // time-correlated removal
    // choose pairs from the union of overall graphs
    boost::shared_ptr<test_pair_vec_t> test_template;
    hac_data_ptr_t Guni;
    {
        int t=0; int arg_pos = t+4;
        // take union, accumulate on the 1st snapshot
        string net_file(argv[arg_pos]);
        Guni = read_upair_file<hac_data_network_t>( net_file.c_str() );
        hac_traits::pair_visitor_t func = func_mk_union(*Guni.get());
        cout << "+" << flush;
        for(int s=1; s<Tmax; ++s){
            cout << "+" << flush;
            arg_pos = s+4;
            hac_data_ptr_t G = read_upair_file<hac_data_network_t>(argv[arg_pos]);
            visit_immediate_pairs(*G.get(),func);
        }
        test_template = select_test_set(Guni,Pmiss,seed);
    }
    cout << "\nDone: made union graph" << endl;

    // visit each snapshot again with a union graph
    // might be messy implementation
    for(int t=0; t<Tmax; ++t){
        cout << "." << flush;
        int arg_pos = t+4;
        string net_file(argv[arg_pos]);
        hac_data_ptr_t G = read_upair_file<hac_data_network_t>( net_file.c_str() );
        hac_data_ptr_t Gtest( new hac_data_network_t() );

        string tmp = lexical_cast<string>( (int) 100*Pmiss );
        string test_out_file = get_fname( net_file ) + "-p" + tmp + "-test.pairs";
        string train_out_file = get_fname( net_file ) + "-p" + tmp + ".pairs";

        // looping through edges in test_template
        // - add weighted edges in Gtest
        // - remove edges in G
        func_mk_test_set ftest(*G.get(),*Gtest.get());
        for_each(test_template->begin(), test_template->end(), ftest);
        func_mk_train_set ftrain(*G.get());
        for_each(test_template->begin(), test_template->end(), ftrain);

        write_upair_file( G, train_out_file.c_str() );
        write_wpair_file( Gtest, test_out_file.c_str() );
    }
    cout << "\nDone: get_dyn_train_set"<< endl;
    return 1;
}

//EOF
