// Measure performance of link-prediction
// (c) 2011, Yongjin Park

#ifndef PERFORMANCE_HH_
#define PERFORMANCE_HH_

#include <iostream>
#include <fstream>
#include <cassert>
#include <vector>
#include <cmath>
#include "util.hh"
#include <algorithm>
#include <boost/smart_ptr/shared_ptr.hpp>
#include <boost/unordered_map.hpp>
#include <boost/tuple/tuple.hpp>

using namespace std;
using namespace boost;

struct link_score_pair_t {
    int u, v;     // (u,v)
    double score; // link score
    int label;    // 0: unknown, -1: negative, 1: positive
    link_score_pair_t(int _u, int _v, double w){
        u = _u; v = _v; score = w; label = 0;
    }
};
typedef boost::shared_ptr<link_score_pair_t> link_ptr_t;


boost::shared_ptr< vector<link_ptr_t> >
read_link_list( const char* link_file, const char* test_file );

template<bool prcurve, bool interpol>
pair< boost::shared_ptr< vector<double> >, boost::shared_ptr< vector<double> > >
make_regular_curve( boost::shared_ptr< vector<link_ptr_t> > pair_list_ptr,
        size_t M, double minval, double maxval );

// make P/R curve
pair< boost::shared_ptr< vector<double> >, boost::shared_ptr< vector<double> > >
make_regular_pr_curve( boost::shared_ptr< vector<link_ptr_t> > pair_list_ptr,
        size_t M, double minval, double maxval );

// make ROC curve
pair< boost::shared_ptr< vector<double> >, boost::shared_ptr< vector<double> > >
make_regular_roc_curve( boost::shared_ptr< vector<link_ptr_t> > pair_list_ptr,
        size_t M, double minval, double maxval );

// Area Under Curve
#define tpzd(a,b,fa,fb) (b-a)*0.5*(fa+fb)
double auc( boost::shared_ptr<vector<double> > xvec_ptr,
        boost::shared_ptr<vector<double> > yvec_ptr );


// ============================================================
// less than operator
// since we want descending order
// this is just opposite
struct link_ptr_lt {
    bool operator() (link_ptr_t ps1,
            link_ptr_t ps2){
        return (ps1->score) > (ps2->score); // opposite
    }
};

#define lex_sort_pair(u,v) if(u>v){ int _tmp=u; u=v; v=_tmp; };

typedef unordered_map< int, link_ptr_t > link_map_t;
typedef boost::shared_ptr< link_map_t > link_map_ptr_t;
typedef unordered_map< int, link_map_ptr_t > link_mom_t;

// make P/R curve
pair< boost::shared_ptr< vector<double> >, boost::shared_ptr< vector<double> > >
make_regular_pr_curve( boost::shared_ptr< vector<link_ptr_t> > pair_list_ptr,
        size_t M, double minval, double maxval ){
    return make_regular_curve<true,true>( pair_list_ptr, M, minval, maxval );
}

// make ROC curve
pair< boost::shared_ptr< vector<double> >, boost::shared_ptr< vector<double> > >
make_regular_roc_curve( boost::shared_ptr< vector<link_ptr_t> > pair_list_ptr,
        size_t M, double minval, double maxval ){
    return make_regular_curve<false,false>( pair_list_ptr, M, minval, maxval );
}



// read link prediction results
// read testing pairs and label links
// and rank pairs by scores (descending order)
boost::shared_ptr< vector<link_ptr_t> >
read_link_list( const char* link_file, const char* test_file ){
    int u,v; double score;
    link_mom_t link_pairs; // map of map of links

    ifstream lin( link_file, ios::in );
    while( lin >> u >> v >> score ){
        if ( u == v ) continue;
        lex_sort_pair(u,v);

        if( link_pairs.count(u) == 0 ) {
            link_map_ptr_t link_map_ptr( new link_map_t );
            link_pairs[u] = link_map_ptr;
        }
        link_map_t& adj_u = *(link_pairs[u].get());

        link_ptr_t link_ptr( new link_score_pair_t(u,v,score) );
        adj_u[v] = link_ptr;
    }
    lin.close();

    boost::shared_ptr< vector<link_ptr_t> > out( new vector<link_ptr_t> );
    vector<link_ptr_t>& pair_list = *out.get();

    // match with test set and label them
    ifstream tin( test_file, ios::in );
    int label;
    while( tin >> u >> v >> label ){
        if ( u == v ) continue;
        lex_sort_pair(u,v);

        if( link_pairs.count(u)==0 ) continue;
        link_map_t& adj_u = *(link_pairs[u].get());
        if( adj_u.count(v) == 0) continue;
        link_ptr_t link_ptr = adj_u[v];
        link_ptr->label = label > 0 ? 1 : -1;
        pair_list.push_back( link_ptr );
    }
    tin.close();

    link_ptr_lt link_lt; // sort them by score
    sort( pair_list.begin(), pair_list.end(), link_lt );

    return out;
}

// make performance curve with regular interval with m intervals
template<bool prcurve, bool interpol>
pair< boost::shared_ptr< vector<double> >, boost::shared_ptr< vector<double> > >
make_regular_curve( boost::shared_ptr< vector<link_ptr_t> > pair_list_ptr,
        size_t M, double minval, double maxval ){

    vector<link_ptr_t>& pair_list = *(pair_list_ptr.get());
    int n = pair_list.size();
    double delta = (maxval-minval) / ((double) M);

    assert( delta > 0. && pair_list.size() > 0 );

    double cutoff = maxval; // best score
    double npos = 0.;
    for(int i=0; i<n; ++i){
        if( pair_list[i]->label > 0 ) ++npos;
    }
    double fp = 0, tp = 0, tn = n - npos, fn = npos;

    vector<double> tp_vec;
    vector<double> fp_vec;

    boost::shared_ptr<vector<double> > x_vec_ptr( new vector<double> );
    boost::shared_ptr<vector<double> > y_vec_ptr( new vector<double> );
    vector<double>& x_vec = *(x_vec_ptr.get());
    vector<double>& y_vec = *(y_vec_ptr.get());

    if( pair_list[0]->label > 0 ){ ++tp; --fn; }
    else if( pair_list[0]->label < 0 ){ ++fp; --tn; }

    // 1st example is good by current cutoff
    if( pair_list[0]->score >= cutoff ){
        tp_vec.push_back(tp);
        fp_vec.push_back(fp);
        if( prcurve && !interpol ){
            double re = tp / (tp+fn);
            double pr = tp / (tp+fp);
            x_vec.push_back(re);
            y_vec.push_back(pr);
        }else if( !prcurve ){
            double fpr = fp / (fp+tn);
            double tpr = tp / (tp+fn);
            x_vec.push_back( fpr );
            y_vec.push_back( tpr );
        }
    }

    int u,v,label; double score;
    for(int i=1; i<n; ++i){
        link_ptr_t link_ptr = pair_list[i];
        link_score_pair_t& linkobj = *(link_ptr.get());
        u = linkobj.u; v = linkobj.v;
        label = linkobj.label; score = linkobj.score;

        // move cutoff until preserving invariant
        // cutoff >= score
        while( cutoff > score && cutoff >=minval ){
            cutoff -= delta;
            tp_vec.push_back(tp);
            fp_vec.push_back(fp);
            if( prcurve && !interpol ){
                double re = tp / (tp+fn);
                double pr = tp / (tp+fp);
                x_vec.push_back(re);
                y_vec.push_back(pr);
            }else if( !prcurve ){
                double fpr = fp / (fp+tn);
                double tpr = tp / (tp+fn);
                x_vec.push_back( fpr );
                y_vec.push_back( tpr );
            }
        }
        // accumulate counts
        if( label > 0 ){ ++tp; --fn; }
        else if( label < 0 ){ ++fp; --tn; }
    }

#ifdef DEBUG
    cerr << "traversed all examples: ";
    cerr << "TP=" << tp << " FP=" << fp  << " delta=" << delta;
    cerr << " nPos=" << npos << " final cutoff=" << cutoff << endl;
#endif

    // interpolate the curve if necessary
    // by Davis and Goadrich, ICML (2006)
    if( prcurve && interpol ){
#ifdef DEBUG
        cerr << "interpolation of P/R curve" << endl;
#endif
        x_vec.push_back( tp_vec[0]/npos );
        y_vec.push_back( tp_vec[0]/(tp_vec[0]+fp_vec[0]) );

        for(int pos=1; pos< tp_vec.size(); ++pos ){
            double dtp = tp_vec[pos] - tp_vec[pos-1];
            if( dtp > 1 ){
                double tpA = tp_vec[pos-1], tpB = tp_vec[pos];
                double fpA = fp_vec[pos-1], fpB = fp_vec[pos];
                for(int x=1; x<=dtp; ++x){
                    double re = (tpA+x) / npos;
                    double slope = (fpB-fpA) / (tpB-tpA);
                    double pr = (tpA+x) / (tpA+x+fpA+slope*x);
                    y_vec.push_back( pr );
                    x_vec.push_back( re );
                }
            }else{
                y_vec.push_back( tp_vec[pos] / (tp_vec[pos]+fp_vec[pos]) );
                x_vec.push_back( tp_vec[pos] / npos );
            }
        }
    }
    pair< boost::shared_ptr<vector<double> >, boost::shared_ptr<vector<double> > > ret( x_vec_ptr, y_vec_ptr );
    return ret;
}


// Area under curve by simple trapezoidal rule
double auc( boost::shared_ptr<vector<double> > xvec_ptr, boost::shared_ptr<vector<double> > yvec_ptr ){
    double ret=0.;
    assert( xvec_ptr->size() > 1 );
    vector<double>& xvec = *(xvec_ptr.get());
    vector<double>& yvec = *(yvec_ptr.get());
    assert( xvec.size() == yvec.size() );

    for(int pos=1; pos<xvec.size(); ++pos ){
        if( xvec[pos-1] < xvec[pos] ){
            ret += tpzd( xvec[pos-1], xvec[pos], yvec[pos-1], yvec[pos] );
        }
    }

    return ret;
}

#endif /* PERFORMANCE_HH_ */
