/////////////////////////////////////////////////////
//
// Dynamic Hierarchical Agglomerative Clustering
//
// (c) 2010-2012, Yongjin Park (ypark28@jhu.edu)
//                Joel S. Bader (joel.bader@jhu.edu)
//
//     Bader Lab, Johns Hopkins University
//
// Distributed under BOOST Software license
// see "boost_1_49_0/LICENSE_1_0.txt"
//
// Source code   : Yongjin Park
// Contributors  : Yongjin Park, Joel Bader
//
/////////////////////////////////////////////////////

#include "alg.hh"


// O(V)
void visit_nodes(
    hac_data_network_t& hac_data,
     hac_traits::node_visitor_t& func ){

    typedef hac_traits traits_t;
    typedef traits_t::graph_t graph_t;
    typedef traits_t::node_t node_t;

    graph_traits<graph_t>::vertex_iterator ui, u_end;

    graph_t& g = *(hac_data.data_graph);
    property_map<graph_t,vertex_name_t>::type node_idx = get(vertex_name, g);

    for(tie(ui,u_end)=vertices(g); ui!=u_end; ++ui){
        node_t u = node_idx[*ui];
        func(u);
    }
}

// O(d)
void visit_immediate_pairs_single(
    hac_data_network_t& hac_data,
     hac_traits::node_t u,
     hac_traits::pair_visitor_t& func ){

#ifdef DEBUG
    assert( hac_data.has_node(u) );
#endif

    typedef hac_traits traits_t;
    typedef traits_t::graph_t graph_t;
    typedef traits_t::node_t node_t;

    graph_t& g = *(hac_data.data_graph);
    property_map<graph_t,vertex_name_t>::type
        node_idx = get(vertex_name, g);

    graph_traits<graph_t>::adjacency_iterator vi, v_end;

    graph_traits<graph_t>::vertex_descriptor
        vertex_u = hac_data.get_vertex(u);
    node_t v;

    for(tie(vi,v_end)=adjacent_vertices(vertex_u,g);
        vi!=v_end; ++vi){
        v = node_idx[*vi];
        func( u, v );
    }
}

// O(V) O(d)
void visit_immediate_pairs( hac_data_network_t& hac_data,
                            hac_traits::pair_visitor_t& func ){

    typedef hac_traits traits_t;
    typedef  traits_t::graph_t graph_t;
    typedef  traits_t::node_t node_t;

    graph_t& g = *(hac_data.data_graph);
     property_map<graph_t,vertex_name_t>::type
        node_idx = get(vertex_name, g);

     graph_traits<graph_t>::vertex_iterator ui, u_end;
     graph_traits<graph_t>::adjacency_iterator vi, v_end;

    node_t u, v;
    for(tie(ui,u_end)=vertices(g); ui!=u_end; ++ui){
        u = node_idx[*ui];
        for(tie(vi,v_end)=adjacent_vertices(*ui,g); vi!=v_end; ++vi){
            v = node_idx[*vi];
            if( u > v ) continue; // skip redundant
            func( u, v );
        }
    }
}


// O(d) O(d)
void visit_2ndorder_pairs_single(
    hac_data_network_t& hac_data,
     hac_traits::node_t u,
     hac_traits::pair_visitor_t& func ){

#ifdef DEBUG
    assert( hac_data.has_node(u) );
#endif

    typedef hac_traits traits_t;
    typedef  traits_t::graph_t graph_t;
    typedef  traits_t::node_t node_t;

    graph_t& g = *(hac_data.data_graph);
     property_map<graph_t,vertex_name_t>::type
        node_idx = get(vertex_name, g);

     graph_traits<graph_t>::vertex_iterator ui, u_end;
     graph_traits<graph_t>::adjacency_iterator vi, v_end;
     graph_traits<graph_t>::adjacency_iterator wi, w_end;

     graph_traits<graph_t>::vertex_descriptor vertex_u;
     graph_traits<graph_t>::edge_descriptor e;
    bool _has_edge;

    // this could be memory overhead...
    unordered_set<node_t> visited(0);

    node_t v, w;

    vertex_u = hac_data.get_vertex(u);

    for(tie(vi,v_end)=adjacent_vertices(vertex_u,g); vi!=v_end; ++vi){
        v = node_idx[*vi];
        // @bug-fix: Wed Sep 21 13:15:39 EDT 2011
        // if( u > v ) continue;       // redundant
        func(u,v);                  // immediate neighbor
        visited.insert(v);
        for(tie(wi,w_end)=adjacent_vertices(*vi,g); wi!=w_end; ++wi){
            w = node_idx[*wi];
            if( u == w ) continue;  // self-loop
            tie(e,_has_edge) = edge(vertex_u,*wi,g);
            if( _has_edge ) continue;// avoid immediate (redundant)
            if( visited.count(w) > 0 ) continue;
            func(u,w);
            visited.insert(w);
        }
    }
}


// O(V) O(d) O(d)
void visit_2ndorder_pairs( hac_data_network_t& hac_data,
                            hac_traits::pair_visitor_t& func ){

    typedef hac_traits traits_t;
    typedef  traits_t::graph_t graph_t;
    typedef  traits_t::node_t node_t;

    graph_t& g = *(hac_data.data_graph);
     property_map<graph_t,vertex_name_t>::type
        node_idx = get(vertex_name, g);

     graph_traits<graph_t>::vertex_iterator ui, u_end;
     graph_traits<graph_t>::adjacency_iterator vi, v_end;
     graph_traits<graph_t>::adjacency_iterator wi, w_end;

     graph_traits<graph_t>::edge_descriptor e;
    bool _has_edge;

    // this could be memory overhead...
    unordered_set<node_t> visited(0);

    node_t u, v, w;
    for(tie(ui,u_end)=vertices(g); ui!=u_end; ++ui){
        u = node_idx[*ui];
        visited.clear();
        for(tie(vi,v_end)=adjacent_vertices(*ui,g); vi!=v_end; ++vi){
            v = node_idx[*vi];
            if( u > v ) continue;       // redundant
                                        // *here you can avoid*
                                        // but not in single op
                                        // {u} spans all
            func(u,v);                  // immediate neighbor
            visited.insert(v);
            for(tie(wi,w_end)=adjacent_vertices(*vi,g); wi!=w_end; ++wi){
                w = node_idx[*wi];
                if( u >= w ) continue;  // redundant and self-loop
                tie(e,_has_edge) = edge(*ui,*wi,g);
                if( _has_edge ) continue;// avoid immediate (redundant)
                if( visited.count(w) > 0 ) continue;
                func(u,w);
                visited.insert(w);
            }
        }
    }
}



// union neighbors for all (x,y) in  (N(u) + N(v)) x (N(u) + N(v))
//                         x < y
// usually to compute some score using data
void
visit_union_neigh_pairs( hac_data_network_t& hac_data,
                         hac_traits::node_t u,
                         hac_traits::node_t v,
                         hac_traits::pair_visitor_t& func ){

    typedef hac_traits traits_t;
    typedef traits_t::graph_t graph_t;
    typedef traits_t::node_t node_t;
    traits_t::node_hash_t& node_hash = *(hac_data.node_hash);
    graph_traits<graph_t>::vertex_descriptor
        vertex_u = node_hash[u], vertex_v = node_hash[v], vertex_x;

    graph_t& g = *(hac_data.data_graph);

    property_map<graph_t,vertex_name_t>::type
        node_idx = get(vertex_name, g);

    graph_traits<graph_t>::adjacency_iterator xi, x_end;

    // construct a vector of union neighbors
    vector<node_t> union_vec;
    // x in neigh(u) - neigh(v)
    node_t x;
    for( tie(xi,x_end)=adjacent_vertices(vertex_u,g); xi!=x_end; ++xi ){
        vertex_x = *xi;
        x = node_idx[vertex_x];
        // skip:  x  s.t.  x = v or x ~ v
        if( x == v || hac_data.has_edge( v, x ) ) continue;
        union_vec.push_back(x);
    }

    // x in neigh(v)
    for( tie(xi,x_end)=adjacent_vertices(vertex_v,g); xi!=x_end; ++xi ){
        vertex_x = *xi;
        x = node_idx[vertex_x];
        // skip: x s.t. x = u
        if( x == u ) continue;
        union_vec.push_back(x);
    }

    // (x,y) in union x union
    for(size_t ii=0; ii<union_vec.size(); ++ii){
        for(size_t jj=(ii+1); jj<union_vec.size(); ++jj ){
            func( union_vec[ii], union_vec[jj] );
        }
    }

}

// union neighbors for all x in  N(u) + N(v)
// usually to compute some score using data
void
visit_data_union_neighbors( hac_data_network_t& hac_data,
                             hac_traits::node_t u,
                             hac_traits::node_t v,
                             hac_traits::union_neigh_visitor_t&
                            func ){

    typedef hac_traits traits_t;
    typedef traits_t::graph_t graph_t;
    typedef traits_t::node_t node_t;
    traits_t::node_hash_t& node_hash = *(hac_data.node_hash);
    graph_traits<graph_t>::vertex_descriptor
        vertex_u = node_hash[u], vertex_v = node_hash[v], vertex_x;

    graph_t& g = *(hac_data.data_graph);

     property_map<graph_t,vertex_name_t>::type
        node_idx = get(vertex_name, g);

     graph_traits<graph_t>::adjacency_iterator xi, x_end;

    double data_u = hac_data.get_node_data(u);
    double data_v = hac_data.get_node_data(v);

    // x in neigh(u) - neigh(v)
    node_t x;
    for( tie(xi,x_end)=adjacent_vertices(vertex_u,g); xi!=x_end; ++xi ){
        vertex_x = *xi;
        x = node_idx[vertex_x];
        // skip:  x  s.t.  x = v or x ~ v
        if( x == v || hac_data.has_edge( v, x ) ) continue;

        double data_x = hac_data.get_node_data(x);
        double data_ux = hac_data.get_edge_data(u, x);
        double data_vx = null_data; // x is only incident to u
        func( x, data_ux, data_vx, data_u, data_v, data_x );
    }

    // x in neigh(v)
    for( tie(xi,x_end)=adjacent_vertices(vertex_v,g); xi!=x_end; ++xi ){
        vertex_x = *xi;
        x = node_idx[vertex_x];
        // skip: x s.t. x = u
        if( x == u ) continue;

        double data_x = hac_data.get_node_data(x);
        double data_vx = hac_data.get_edge_data(v, x);
        if( hac_data.has_edge( u, x) ){
            double data_ux = hac_data.get_edge_data(u, x);
            func( x, data_ux, data_vx, data_u, data_v, data_x );
        } else {
            double data_ux = null_data;
            func( x, data_ux, data_vx, data_u, data_v, data_x );
        }
    }
}


// visit other neighbors not in union neighbors of u and v
void
visit_data_inv_union_neighbors(
    hac_data_network_t& hac_data,
     hac_traits::node_t u,
     hac_traits::node_t v,
     hac_traits::union_neigh_visitor_t& func ){

    typedef hac_traits traits_t;
    typedef  traits_t::graph_t graph_t;
    typedef  traits_t::node_t node_t;
    // traits_t::node_hash_t& node_hash = *(hac_data.node_hash);
     graph_traits<graph_t>::vertex_descriptor vertex_x;
    //        vertex_u = node_hash[u], vertex_v = node_hash[v], 

    graph_t& g = *(hac_data.data_graph);

     property_map<graph_t,vertex_name_t>::type
        node_idx = get(vertex_name, g);

    double data_u = hac_data.get_node_data(u);
    double data_v = hac_data.get_node_data(v);
    // double data_x, data_ux, data_vx;

    // x not in N(u)+N(v)
     graph_traits<graph_t>::vertex_iterator xi, x_end;
    for( tie(xi,x_end)=vertices(g); xi!=x_end; ++xi ){
        vertex_x = *xi;
        node_t x = node_idx[vertex_x];
        // skip:  x  s.t.  x ~ u or x ~ v
        if( hac_data.has_edge( v, x )
            || hac_data.has_edge( u, x ) ) continue;
        if( x == v || x == u ) continue;

        double data_x = hac_data.get_node_data(x);
        double data_ux = null_data;
        double data_vx = null_data;
        func( x, data_ux, data_vx, data_u, data_v, data_x );
    }
}


#ifdef DEBUG
// not quite useful...
// also for debugging
void visit_data_edges( hac_data_network_t& hac_data,
                       hac_data_network_t::edge_visitor_t& func ){


    typedef hac_traits traits_t;
    typedef traits_t::graph_t graph_t;
    typedef traits_t::node_t node_t;
    typedef hac_data_network_t::children_vec_t node_set_t;

    graph_t& g = *(hac_data.data_graph);

    property_map<graph_t,children_under_t>::type
       children = get(children_under_t(),g);

    graph_traits<graph_t>::edge_iterator ei, e_end;
    property_map<graph_t,vertex_name_t>::type
        node_idx = get(vertex_name, g);

    graph_traits<graph_t>::edge_descriptor e;
    graph_traits<graph_t>::vertex_descriptor u, v;

    for(tie(ei,e_end)=edges(g); ei!=e_end; ++ei){
        e = *ei;
        u = source(e,g);
        v = target(e,g);
        double edata = hac_data.get_edge_data(node_idx[u],node_idx[v]);
        double vdata_u = hac_data.get_node_data(node_idx[u]);
        double vdata_v = hac_data.get_node_data(node_idx[v]);
        traits_t::node_vec_t* set_u = get(children,u);
        traits_t::node_vec_t* set_v = get(children,v);
        // call visitor, functor
        func( edata, vdata_u, vdata_v, *set_u, *set_v );
    }
}

// mainly for debugging purpose
// sum node data of a set
double
sum_nodes_within( hac_data_network_t::children_vec_t& set_u,
                  hac_data_network_t& other_hac_data ){

    double ret = null_data;

    hac_data_network_t::children_vec_t::iterator it;
    for(it=set_u.begin(); it!=set_u.end(); ++it){
        ret += other_hac_data.get_node_data(*it);
    }
    return ret;
}

// mainly for debugging purpose
// sum edge data between two sets on (perhaps other?) data
double
sum_edges_between( hac_data_network_t::children_vec_t& set_u,
                   hac_data_network_t::children_vec_t& set_v,
                   hac_data_network_t& other_hac_data ){

    double ret = null_data;
    hac_data_network_t::children_vec_t::iterator it, jt;

    for(it=set_u.begin(); it!=set_u.end(); ++it){
        for(jt=set_v.begin(); jt!=set_v.end(); ++jt){
            if( other_hac_data.has_edge(*it,*jt) )
                ret += other_hac_data.get_edge_data(*it,*jt);
        }
    }
    return ret;
}
#endif

//EOF
