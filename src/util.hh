/////////////////////////////////////////////////////
//
// Dynamic Hierarchical Agglomerative Clustering
//
// (c) 2010-2012, Yongjin Park (ypark28@jhu.edu)
//                Joel S. Bader (joel.bader@jhu.edu)
//
//     Bader Lab, Johns Hopkins University
//
// Distributed under BOOST Software license
// see "boost_1_49_0/LICENSE_1_0.txt"
//
// Source code   : Yongjin Park
// Contributors  : Yongjin Park, Joel Bader
//
/////////////////////////////////////////////////////

#ifndef DHAC_UTIL_HH
#define DHAC_UTIL_HH

#include <fstream>
#include <ctime>
#include <vector>
#include <string>
#include <cstring>
#include <boost/multi_array.hpp>
#include <boost/lexical_cast.hpp>
#include <sstream>

using namespace boost;
using namespace std;


// boost multi array
template<typename T>
void init_vec(vector<T>& vec, size_t K);

template<typename T>
void init_mat(multi_array<T,2>& mat, size_t nRows, size_t nCols);

////////////////////////////////////////////////////////////////
// Simple math
// safe logarithm
inline
double safe_log(double val);

// output log-likelihood trace
struct func_llik_out{
    func_llik_out(ofstream& _fout): fout(_fout) {}
    void operator() (double val){ fout << val << endl; }
    ofstream& fout;
};

void write_llik_vec( const char* o_hdr, vector<double>* llik_vec );

////////////////////////////////////////////////////////////////
// a bit formatted output
// Debugging Message
#define DebugMsgNL(_msg) cerr << "+ Debug: " << _msg << endl;
#define DebugMsg(_msg) cerr << "+ Debug: " << _msg;

// Error reporting
#define ErrMsgNL(_msg) cerr << "+ Error: " << _msg << endl;
#define RetErr(_msg) { cerr << "+ Error: " << _msg << endl; return -1; }

// leave some log 
#define LogMsgNL(_msg){ cerr << "++ " << _msg << endl; }
#define LogMsg(_msg){ cerr << "++ " << _msg << flush; }



////////////////////////////////////////////////////////////////
// Get Time String
//
void _getTimeStr( char* dstr, char* tstr, time_t tt );

string get_curr_time();

// Measure time interval
// - basically same as Matlab's tic&toc
// Note: not thread-safe
static time_t _Time_Tic;
void tic();
int toc();

////////////////////////////////////////////////////////////////
// string
string get_fname( string& file_str );
string get_ext( string& file_str );

#endif

