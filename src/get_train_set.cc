/////////////////////////////////////////////////////
//
// Dynamic Hierarchical Agglomerative Clustering
//
// (c) 2010-2011, Yongjin Park (ypark28@jhu.edu)
//                Joel S. Bader (joel.bader@jhu.edu)
//
//     Bader Lab, Johns Hopkins University
//
// Distributed under BOOST Software license
// see "boost_1_49_0/LICENSE_1_0.txt"
//
// Source code   : Yongjin Park
// Contributors  : Yongjin Park, Joel Bader
//
/////////////////////////////////////////////////////


#include "sample.hh"


// sample training/testing set
// [exe] Gtot.pairs Pmiss seed
int main(int argc, const char* argv[]){
    if(argc < 3){
        cout << "get_train_set G.pairs Pmiss [seed]" << endl;
        return -1;
    }
    double Pmiss = lexical_cast<double>(argv[2]);
    int seed = lexical_cast<int>(argv[3]);
    if( Pmiss <= 0 || Pmiss >= 1. ){
        cout << "Pmiss should be in (0,1)" << endl; return -1;
    }

    make_test_set( string(argv[1]), Pmiss, seed );
    return 1;
}
