/////////////////////////////////////////////////////
//
// Dynamic Hierarchical Agglomerative Clustering
//
// (c) 2010-2012, Yongjin Park (ypark28@jhu.edu)
//                Joel S. Bader (joel.bader@jhu.edu)
//
//     Bader Lab, Johns Hopkins University
//
// Distributed under BOOST Software license
// see "boost_1_49_0/LICENSE_1_0.txt"
//
// Source code   : Yongjin Park
// Contributors  : Yongjin Park, Joel Bader
//
/////////////////////////////////////////////////////

#include "hac.hh"


// ==========================================================
void print_help(){
    cerr << "\n<usage>" << endl;
    cerr << "./hacml name scope data1.pairs data2.pairs ..." << endl;
    cerr << " name     = experiment name (string)" << endl;
    cerr << " scope    = (1: immediate, 2: neighbors of neighbors)" << endl;
    cerr << " *.pairs  = data files" << endl;
    cerr << "            where lines contain edges" << endl;
    cerr << "              u <\\t> v <\\n>" << endl;
    cerr << "            with integer-valued nodes" << endl;
    cerr << endl;
    cerr << "HAC-ML (c) 2010-2012 Yongjin Park and Joel S. Bader" << endl;
    cerr << "       {ypark28, joel.bader}@jhu.edu" << endl;
    cerr << endl;
}

// ==========================================================
int main(int argc, const char* argv[]){

    if(argc < 4){ print_help(); return -1; }

    cerr << "Start: HAC-ML" << endl;

    string name( argv[1] );
    int scope = atoi(argv[2]);
    bool approx = (scope == 1) ? true : false;
    int Dmax = argc - 3; // argv[3], argv[4], ...

    double tot_max_score = 0.;

    hac_data_ptr_vec_t data_vec;

    // read relevant data graphs
    for(int t=0; t<Dmax; ++t){
        int curr_arg_pos = t+3;
        data_vec.push_back( 
            read_upair_file<hac_data_network_t>(argv[curr_arg_pos]) );            
    }

    // 1st pass
    score_vec_ptr_t score_vec_ptr;
    merge_pair_vec_ptr_t merge_pair_vec_ptr;
    
    tie(score_vec_ptr, merge_pair_vec_ptr) =
        run_hac_global(data_vec, approx);

    // find maximum and argmax K
    score_vec_t& score_vec = *score_vec_ptr.get();
    int tot_merges = score_vec.size();
    cerr << "Total #merges = " << tot_merges << endl;

    int argmax = 0;
    double max_score = 0;

    for(int pos=1; pos<tot_merges; ++pos){
        if( score_vec[pos] > max_score )
        { argmax=pos; max_score=score_vec[pos]; }
    }
    cerr << "Best found at #merges = " << argmax << endl;
    data_vec.clear();


    // 1. overall group structure
#ifdef DEBUG
    // using 2nd pass
    // explicitly read relevant data graphs
    for(int t=0; t<Dmax; ++t){
        int curr_arg_pos = t+3;
        data_vec.push_back( read_upair_file<hac_data_network_t>(argv[curr_arg_pos]) );
    }

    hac_data_ptr_t rep_data_ptr = 
        run_hac_global_2nd( data_vec, merge_pair_vec_ptr, argmax );

    write_group_info( rep_data_ptr, (name + "-debug.group").c_str() );
#endif

    // group output given merge order
    write_group_info( *(merge_pair_vec_ptr.get()), argmax,
                      (name+".group").c_str() );

    // 2. merge order
    {
        int u,v;
        ofstream fout( (name+".merge").c_str() );
        merge_pair_vec_t& mvec = *(merge_pair_vec_ptr.get());
        for(int r=0; r<mvec.size(); ++r){
            tie(u,v) = *(mvec[r].get());
            fout << u << "\t" << v << endl;
        }
    }

    data_vec.clear();
    cerr << "Done: HAC-ML" << endl;
    return 1;
}
// EOF
