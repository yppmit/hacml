//
// link_pred_group.cc
// (c) Yongjin Park, Mar 26, 2012
//

#include "link_pred_group.hh"

// sample link by group
// [exe] Gtrain.pairs group Gtest.wpairs
int main(int argc, const char* argv[]){
    if(argc < 3){
        cerr << "link_pred_group train.group train.pairs" << endl;
        cerr << "STDIN selected pairs" << endl;
        cerr << "STDOUT prediction results" << endl;
        return -1;
    }


    hac_data_ptr_t hac_data_ptr = read_upair_file<hac_data_network_t>( argv[2] );
    hac_data_network_t& Gtrain = *(hac_data_ptr.get());

    // selected pairs
    // store them in a graph
    hac_data_network_t Gtest;
    {
        int u,v;
        vector<int> vec1; vector<int> vec2;
        while(cin >> u >> v){        
            Gtest.add_node(u,0.); Gtest.add_node(v,0.);
            vec1.push_back(u); vec2.push_back(v);
        }
        
        for(int r=0; r<vec1.size(); ++r){
            Gtest.add_data_edge(vec1[r],vec2[r],0.);
        }
    }

    make_link_group( Gtrain, Gtest, argv[1] );
    return 0;
}
