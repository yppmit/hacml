/////////////////////////////////////////////////////
//
// Dynamic Hierarchical Agglomerative Clustering
//
// (c) 2010-2012, Yongjin Park (ypark28@jhu.edu)
//                Joel S. Bader (joel.bader@jhu.edu)
//
//     Bader Lab, Johns Hopkins University
//
// Distributed under BOOST Software license
// see "boost_1_49_0/LICENSE_1_0.txt"
//
// Source code   : Yongjin Park
// Contributors  : Yongjin Park, Joel Bader
//
/////////////////////////////////////////////////////

#include "hacvf.hh"


// ==========================================================
void print_help(){
    cerr << "\n<usage>" << endl;
    cerr << "./hacmlvf -n name -s scope -d {data.pairs} -f {feature.txt}" << endl;
    cerr << " -n name          : experiment name (string)" << endl;
    cerr << " -s scope         : 1=immediate neighbors (default)" << endl;
    cerr << "                    2=neighbors of neighbors" << endl;
    cerr << " -d {data.pairs}  : network data files" << endl;
    cerr << "                    each line = u tab v" << endl;
    cerr << " -f {feature.txt} : vertex feature files" << endl;
    cerr << "                    each line = u" << endl;
    cerr << endl;
    cerr << "HAC-ML (c) 2010-2012 Yongjin Park and Joel S. Bader" << endl;
    cerr << "       {ypark28, joel.bader}@jhu.edu" << endl;
    cerr << endl;
}

struct cmd_input_t {
    vector<string> data_files;
    vector<string> feature_files;
    bool approx;
    int Dmax;
    int Fmax;
    string name;
};

int parse_cmd( int argc, const char* argv[], cmd_input_t& cmd_input ){

    // default options
    cmd_input.Dmax = 0;
    cmd_input.Fmax = 0;
    cmd_input.name = "";
    cmd_input.approx = true;

    string curr;
    for(int pos=1; pos < argc; ++pos){
        curr = argv[pos];
        if( curr == "-n" ){
            if( ++pos >= argc ){ return -1; }
            cmd_input.name = argv[pos];
        }else if( curr == "-s" ){
            if( ++pos >= argc ){ return -1; }
            cmd_input.approx = (2!=lexical_cast<int>(argv[pos]));
        }else if( curr == "-d" ){
            if( ++pos >= argc ){ return -1; }
            cmd_input.data_files.push_back( argv[pos] );
            cmd_input.Dmax++;
        }else if( curr == "-f" ){
            if( ++pos >= argc ){ return -1; }
            cmd_input.feature_files.push_back( argv[pos] );
            cmd_input.Fmax++;
        }
    }

    if( cmd_input.Dmax < 1 ) return -1;

    cerr << "# Data files         = " << cmd_input.Dmax << endl;
    cerr << "# Feature files      = " << cmd_input.Fmax << endl;
    cerr << "Extensive clustering = " << (cmd_input.approx? "no" : "yes") << endl;
    cerr << "Experiment name      = " << cmd_input.name << endl;

    return 1;
}


// ==========================================================
int main(int argc, const char* argv[]){

    cerr << "Start: HAC-ML with vertex features" << endl;

    cmd_input_t cmd_input;
    if( parse_cmd( argc, argv, cmd_input ) < 1 ){ print_help(); return -1; }

    string name = cmd_input.name;
    bool approx = cmd_input.approx;
    int Dmax = cmd_input.Dmax;
    int Fmax = cmd_input.Fmax;
    vector<string>& dfiles = cmd_input.data_files;
    vector<string>& ffiles = cmd_input.feature_files;

    hac_data_ptr_vec_t data_vec;

    // read relevant data graphs
    for(int t=0; t<Dmax; ++t)
        data_vec.push_back( read_upair_file<hac_data_network_t>(dfiles[t].c_str()) );

    vf_hac_data_ptr_t rep_ptr = mk_union_data( data_vec );

    // read feature files
    for(int t=0; t<Fmax; ++t)
        add_vertex_feature_file( *(rep_ptr.get()), ffiles[t].c_str() );

// #ifdef DEBUG
//     dump_feature_mat( *(rep_ptr.get()) );
// #endif

    // determine full merging order
    score_vec_ptr_t score_vec_ptr;
    merge_pair_vec_ptr_t merge_pair_vec_ptr;
    tie(score_vec_ptr, merge_pair_vec_ptr) = run_hac_global(rep_ptr, data_vec, approx);

// #ifdef DEBUG
//     dump_feature_mat( *(rep_ptr.get()) );
// #endif

    // find maximum and argmax K
    score_vec_t& score_vec = *score_vec_ptr.get();
    int tot_merges = score_vec.size();

    int argmax = 0; double max_score = 0;
    for(int pos=1; pos<tot_merges; ++pos){
        if( score_vec[pos] > max_score )
        { argmax=pos; max_score=score_vec[pos]; }
    }
    data_vec.clear();

    // 1. group structure
    write_group_info( *(merge_pair_vec_ptr.get()), argmax,
                      (name+".group").c_str() );

    // 2. merge order
    {
        int u,v;
        ofstream fout( (name+".merge").c_str() );
        merge_pair_vec_t& mvec = *(merge_pair_vec_ptr.get());
        for(size_t r=0; r<mvec.size(); ++r){
            tie(u,v) = *(mvec[r].get());
            fout << u << "\t" << v << endl;
        }
    }

    cerr << "Done: HAC-ML with vertex features" << endl;
    return 1;
}
// EOF
