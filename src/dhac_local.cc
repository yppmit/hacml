/////////////////////////////////////////////////////
//
// Dynamic Hierarchical Agglomerative Clustering
//
// (c) 2010-2012, Yongjin Park (ypark28@jhu.edu)
//                Joel S. Bader (joel.bader@jhu.edu)
//
//     Bader Lab, Johns Hopkins University
//
// Distributed under BOOST Software license
// see "boost_1_49_0/LICENSE_1_0.txt"
//
// Source code   : Yongjin Park
// Contributors  : Yongjin Park, Joel Bader
//
/////////////////////////////////////////////////////

#include "dhac.hh"


// ==========================================================
void print_help(){
    cerr << "\n<usage>" << endl;
    cerr << "dhac_local bmin bmax s data1.pairs data2.pairs ..." << endl;
    cerr << " bmin   = min bandwidth (>0)" << endl;
    cerr << " bmax   = max bandwidth (>0)" << endl;
    cerr << " s      = scope (1: immediate, 2: neighbors of neighbors)" << endl;
    cerr << " .pairs = temporally ordered data files" << endl;
    cerr << "          where each line contains" << endl;
    cerr << "            u <\\t> v <\\n>" << endl;
    cerr << "          with integer-valued nodes" << endl;
    cerr << "          file names must be distinguishable" << endl;
    cerr << endl;
    cerr << "DHAC (c) 2011-2012 Yongjin Park (ypark28@jhu.edu)" << endl;
}

// ==========================================================
int main(int argc, const char* argv[]){

    if(argc < 5){ print_help(); return -1; }

    cerr << "Start: DHAC-local" << endl;

    double bmin = atof(argv[1]);
    double bmax = atof(argv[2]);
    int scope = atoi(argv[3]);
    bool approx = (scope == 1) ? true : false;
    int Tmax = argc - 4; // argv[4], argv[5], ...

    double binter = abs(bmax-bmin)/5.;

    for(int t=0; t<Tmax; ++t){
        int curr_arg_pos = t+4;
        
        cerr << "\n\n Snapshot t = " << t << "\n\n" << endl;

        hac_data_ptr_vec_t data_vec;
        vector<double> kern_weights;
        
        // 1st pass: choose best bandwidth
        double max_score_bw=0.;
        double argmax_bw=bmin;
        int argmax_pos=0;
        merge_pair_vec_ptr_t max_merge_pair_vec_ptr;
        for(double bw=bmin; bw<=bmax; bw+=binter){

            data_vec.clear();
            kern_weights.clear();
            kernel_t kern(t,Tmax,bw);

            cerr << "Bandwidth = " << bw << endl;
            // read relevant data points
            // current snapshot and weight
            data_vec.push_back( read_upair_file<hac_data_network_t>(argv[curr_arg_pos]) );
            kern_weights.push_back( kern.val(t) );
            // relevant snapshots with weights
            for(int s=0; s<Tmax; ++s){
                if( s==t ) continue;
                double w = kern.val(s);
                int rel_arg_pos = 4+s;
                if( w < kern_cutoff ){ continue; }
                data_vec.push_back( read_upair_file<hac_data_network_t>(argv[rel_arg_pos]) );
                kern_weights.push_back(w);
            }
            
            score_vec_ptr_t score_vec_ptr;
            merge_pair_vec_ptr_t merge_pair_vec_ptr;

            tie(score_vec_ptr, merge_pair_vec_ptr) =
                    run_hac_global(data_vec, kern_weights, approx);

            // find maximum and argmax K
            score_vec_t& score_vec = *score_vec_ptr.get();
            int tot_merges = score_vec.size();
            cerr << "Total #merges = " << tot_merges << endl;
            int curr_argmax_pos = 0;
            double curr_max_score = 0.;
            for(int pos=1; pos<tot_merges; ++pos){
                if( score_vec[pos] > curr_max_score )
                { curr_argmax_pos=pos; curr_max_score=score_vec[pos]; }
            }
            cerr << "Current best found at #merges = " << curr_argmax_pos << endl;

            if( max_score_bw < curr_max_score ){
                max_score_bw = curr_max_score;
                argmax_bw = bw;
                argmax_pos = curr_argmax_pos;
                max_merge_pair_vec_ptr = merge_pair_vec_ptr;
            }
        }

        // shared_ptr will be cleared out of memory
        data_vec.clear(); kern_weights.clear();

        cerr << "Best bandwidth = " << argmax_bw << endl;

        string data_file(argv[curr_arg_pos]);

#ifdef DEBUG
        // 2nd pass with best kernel bandwidth
        // current snapshot and weight
        kernel_t kern(t,Tmax,argmax_bw);
        data_vec.push_back( read_upair_file<hac_data_network_t>(argv[curr_arg_pos]) );
        kern_weights.push_back( kern.val(t) );

        if( max_score_bw > 0. ){
            // read relevant data points
            for(int s=0; s<Tmax; ++s){
                if( s==t ) continue;
                int rel_arg_pos = 4+s;
                double w = kern.val(s);
                if( w < kern_cutoff ){ continue; }
                data_vec.push_back( read_upair_file<hac_data_network_t>(argv[rel_arg_pos]) );
                kern_weights.push_back(w);
            }

            run_hac_global_2nd( data_vec, kern_weights, max_merge_pair_vec_ptr, argmax_pos );
        }else{
            cerr << "No cluster/community found!" << endl;
        }

        // output
        write_group_info( data_vec[0], (get_fname(data_file) + "-debug.group").c_str() );
#endif
        hac_data_ptr_t Gptr = read_upair_file<hac_data_network_t>( argv[curr_arg_pos] );
        write_group_info<hac_data_network_t>(*(max_merge_pair_vec_ptr.get()), 
                                             *(Gptr.get()), argmax_pos,
                                             (get_fname(data_file) + ".group").c_str());

    }

    cerr << "Done: DHAC-local" << endl;
    return 1;
}
// EOF
