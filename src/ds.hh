/////////////////////////////////////////////////////
//
// Dynamic Hierarchical Agglomerative Clustering
//
// (c) 2010-2012, Yongjin Park (ypark28@jhu.edu)
//                Joel S. Bader (joel.bader@jhu.edu)
//
//     Bader Lab, Johns Hopkins University
//
// Distributed under BOOST Software license
// see "boost_1_49_0/LICENSE_1_0.txt"
//
// Source code   : Yongjin Park
// Contributors  : Yongjin Park, Joel Bader
//
/////////////////////////////////////////////////////

#ifndef DHAC_DATA_STRUCTURE_HH
#define DHAC_DATA_STRUCTURE_HH

#include "util.hh"

#include <iostream>
#include <cassert>
#include <climits>
#include <iostream>
#include <utility>
#include <fstream>
#include <algorithm>
#include <vector>
#include <map>
#include <set>
#include <cstdlib>

using namespace std;

// boost graph
#include <boost/graph/graph_traits.hpp>
#include <boost/property_map/property_map.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/unordered_set.hpp>
#include <boost/unordered_map.hpp>
#include <boost/function.hpp>
#include <boost/shared_ptr.hpp>

using namespace boost;

// vertex properties
// 1. number of observations
// 2. accumulated edge data under vertex
struct vertex_weight_t { typedef boost::vertex_property_tag kind; };
struct edges_under_t { typedef boost::vertex_property_tag kind; };

#ifdef DEBUG
// 3. a vector of objects (for output)
struct children_under_t { typedef boost::vertex_property_tag kind; };
#endif

// // 4. with vertex feature vector
// struct vertex_feature_t { typedef vertex_property_tag kind; };
// typedef vector<double> vfeature_vec_t;



// edge properties
// 1. number of observations
// 2. accumulated edge weights
// (possible extensions, 2nd order statistics)
struct edge_count_t { typedef boost::edge_property_tag kind; };


class hac_data_network_t {

public:
  // node_t: external access
  // "node" is for index, "vertex" is boost graph vertex
  typedef int node_t;
#ifdef DEBUG
  typedef vector<node_t> children_vec_t;
#endif

  static const bool has_feature = false;

protected:

  // Undirected graph type
#ifdef DEBUG
  typedef adjacency_list<
  setS, setS,  // store out-edges set, vertex set
  undirectedS,           // undirected graph
  property<vertex_index_t, size_t,
	   property<vertex_name_t, int,
		    property<vertex_weight_t, double,
			     property<edges_under_t, double, 
				      property<children_under_t, children_vec_t* > > > > >,
  property<edge_index_t, size_t, 
	   property<edge_weight_t, double,
		    property<edge_count_t, double > > >
  > data_graph_t;
#else
  typedef adjacency_list<
    hash_setS, hash_setS,  // store out-edges set, vertex set
    undirectedS,           // undirected graph
    property<vertex_index_t, size_t,
	     property<vertex_name_t, int,
		      property<vertex_weight_t, double,
			       property<edges_under_t, double> > > >,
    property<edge_index_t, size_t, 
	     property<edge_weight_t, double,
		      property<edge_count_t, double > > >
    > data_graph_t;
#endif

  // data_vertex_t: internal access
  typedef graph_traits<data_graph_t>::vertex_descriptor
  data_vertex_t;
  // data_edge_t: internal access
  typedef graph_traits<data_graph_t>::edge_descriptor
  data_edge_t;

  // hash: node_t -> data_vertex_t
  typedef boost::unordered_map<node_t, data_vertex_t> node_hash_t;

  // ================ End of definitions ================


  // data structures
  data_graph_t* data_graph;          // backbone graph
  node_hash_t* node_hash;            // map: node_t u -> vertex_t


  double tot_node_data;              // accum of all node data
  double tot_edge_data;              // accum of all edge data

  node_t max_node_idx;


public:
  ////////////////
  // methods
  // memory allocation and de-allocation
  hac_data_network_t(){
    data_graph = new data_graph_t();
    node_hash = new node_hash_t();
    tot_node_data = 0.;
    tot_edge_data = 0.;

    max_node_idx = 0;
  }
    
  ~hac_data_network_t(){
#ifdef DEBUG
    // clear children under each vertex
    data_graph_t::vertex_iterator vi, vEnd;
    for(tie(vi,vEnd)=vertices(*data_graph); vi!=vEnd; ++vi){
      delete get(children_under_t(),*data_graph,*vi);
    }
#endif
    // clear out graph, and hash
    delete data_graph;
    delete node_hash;
  }

  // ==============================================================
  // has, add, del node
  bool has_node( node_t u ){
    return node_hash->count(u) > 0 ;
  }

  int add_node( node_t u, double data ){
    if( has_node(u) ) return EXIT_FAILURE;
    if( u > max_node_idx ){ max_node_idx=u; }
    node_hash_t& hash = *node_hash;
    data_graph_t& G = *data_graph;

    data_vertex_t vertex_u = boost::add_vertex(*data_graph);
    hash[u] = vertex_u;                       // u -> vetex_t
    boost::put(vertex_name, G, vertex_u, u);         // vertex name
    boost::put(vertex_weight_t(),G,vertex_u,data);   // # objects under u
    boost::put(edges_under_t(),G,vertex_u,0);        // 0 edges under u
#ifdef DEBUG
    children_vec_t* vec = new children_vec_t; // under u
    if( abs(data) > 0 ) vec->push_back(u);    // 'u' is only object
    boost::put(children_under_t(), G, vertex_u,vec); // update property map
#endif
    tot_node_data += data;                    // adjust total data
    return EXIT_SUCCESS;
  }

  double get_node_data( node_t u ){
#ifdef DEBUG
    assert(has_node(u));
#endif
    return boost::get(vertex_weight_t(),*data_graph,get_vertex(u));
  }

  double get_edge_under( node_t u ){
#ifdef DEBUG
    assert(has_node(u));
#endif
    return boost::get(edges_under_t(),*data_graph,get_vertex(u));
  }

#ifdef DEBUG
  children_vec_t* get_children( node_t u ){
    assert(has_node(u));
    return boost::get(children_under_t(),*data_graph,get_vertex(u));
  }
#endif

  // delete this node
  int del_node( node_t u ){
    if(!has_node(u)) return EXIT_FAILURE;
    node_hash_t& hash = *node_hash;
    data_vertex_t vertex_u = hash[u];
    double data = get_node_data(u);
    tot_node_data -= data;
#ifdef DEBUG
    delete get_children(u);
#endif
    data_graph_t& g = *data_graph;

    // adjust total edge data 
    // subtracting out edges attached to u
    data_graph_t::out_edge_iterator ei, e_end;
    for( tie(ei,e_end)=out_edges(vertex_u, g);
	 ei != e_end; ++ei ){
      tot_edge_data -= get( edge_weight, g, *ei );
    }

    boost::clear_vertex(vertex_u, g);
    boost::remove_vertex(vertex_u, g);

    hash.erase(u);
            
    return EXIT_SUCCESS;
  }

  // ==============================================================
  // has, add, del edge

  bool has_edge( node_t u, node_t v ){
    //#ifdef DEBUG
    //        cerr << "has_edge(" << u << "," << v << ")" << endl;
    //#endif
    if( !has_node(u) || !has_node(v) ) return false;
    node_hash_t& hash = *node_hash;
    data_vertex_t vertex_u = hash[u];
    data_vertex_t vertex_v = hash[v];
    data_edge_t edge_uv; bool ok;
    tie(edge_uv,ok) = boost::edge(vertex_u, vertex_v, *data_graph);
    return (ok);
  }

  int add_data_edge( node_t u, node_t v, double data ){
    if( u == v ) return EXIT_FAILURE;
    if( !has_node(u) || !has_node(v) ){
      ErrMsgNL("nodes " << u << ", " << v << " not present");
      return EXIT_FAILURE;
    }
    if( has_edge(u,v) ){
      // ErrMsgNL("edge (" << u << ", " << v << ") present");
      return EXIT_FAILURE;
    }
    node_hash_t& hash = *node_hash;
    data_vertex_t vertex_u = hash[u];
    data_vertex_t vertex_v = hash[v];
    data_edge_t edge_uv; bool ok;
    data_graph_t& g = *data_graph;
    tie( edge_uv, ok ) = boost::add_edge( vertex_u, vertex_v, g );
    boost::put( edge_weight, g, edge_uv, data );
    boost::put( edge_count_t(), g, edge_uv, 1. );
#ifdef DEBUG
    assert( ok );
#endif
    tot_edge_data += data;
    return EXIT_SUCCESS;
  }

  double get_edge_data( node_t u, node_t v ){
#ifdef DEBUG
    assert( has_node(u) && has_node(v) );
#endif
    node_hash_t& hash = *node_hash;
    data_edge_t edge_uv; bool ok;
    tie(edge_uv, ok) = boost::edge( hash[u], hash[v], *data_graph );
    if(ok) return get( edge_weight, *data_graph, edge_uv );
    return 0.;
  }

  void set_edge_data( node_t u, node_t v, double data ){
    if( !has_edge(u,v) ){ add_data_edge(u,v,data); }
    else{
      data_edge_t edge_uv = get_edge(u,v);
      boost::put( edge_weight, *data_graph, edge_uv, data );
    }
  }

  void del_edge( node_t u, node_t v ){
#ifdef DEBUG
    assert( has_edge(u,v) );
#endif
    double data = get_edge_data(u,v);
    tot_edge_data -= data;
    node_hash_t& hash = *node_hash;
    boost::remove_edge( hash[u], hash[v], *data_graph );        
  }

  // ========= merge nodes: u+v -> u ==========
  node_t contract_nodes( node_t u, node_t v ){
#ifdef DEBUG
    double tot_node_data_org = tot_node_data;
    double tot_edge_data_org = tot_edge_data;
    if ( u == v ) { ErrMsgNL("u==v"); }
    assert( has_node(u) && has_node(v) );
    assert( u!=v );
#endif

    // several *erroneous* cases 
    // u == v.
    //     just do nothing
    // u != v, but u does not exist and v exists.
    //     transfer v to u
    // u != v, but u exists and v does not.
    //     just do nothing


    node_hash_t& hash = *node_hash;
    data_vertex_t vertex_u = hash[u];
    data_vertex_t vertex_v = hash[v];
    data_vertex_t vertex_w;


    data_graph_t& G = *data_graph;

    double edge_count_uv = 0., edge_data_uv = 0.;
    graph_traits<data_graph_t>::edge_descriptor e_uv, e_vw, e_uw;
    bool ok;
    if( has_edge(u,v) ){
      e_uv = get_edge(u,v);
      edge_count_uv = get(edge_count_t(),G,e_uv);
      edge_data_uv = get(edge_weight,G,e_uv);
      del_edge(u,v);
    }

    property_map<data_graph_t,edge_weight_t>::type edge_weight_map = get(edge_weight,G);
    property_map<data_graph_t,edge_count_t>::type edge_count_map = get(edge_count_t(),G);

    // accumulate edge_data (v, w) + (u, w) -> (u', w)
    // neighbors of v
    data_graph_t::adjacency_iterator w_it, w_end;
    for( tie(w_it, w_end) = adjacent_vertices(vertex_v,G);
	 w_it != w_end; ++w_it ){
      vertex_w = *w_it;
      //node_t w = get(vertex_index,*data_graph,*w_it);
#ifdef DEBUG
      assert( vertex_w != vertex_u ); // because edge (u,v) deleted
#endif

      tie(e_vw,ok) = boost::edge(vertex_v,vertex_w,G);
      double data = get(edge_weight_map,e_vw);
      double cnt = get(edge_count_map,e_vw);

      // u ~ w
      tie( e_uw, ok ) = boost::edge(vertex_u, vertex_w, G);
      if( ok ) {
	double data0 = get( edge_weight_map, e_uw );
	double cnt0 = get( edge_count_map, e_uw );

	boost::put(edge_weight_map,e_uw,data0+data);
	boost::put(edge_count_map,e_uw,cnt0+cnt);
      }else{
	tie( e_uw, ok ) = boost::add_edge( vertex_u, vertex_w, G );
	boost::put( edge_weight_map, e_uw, data );
	boost::put( edge_count_map, e_uw, cnt );
      }
      tot_edge_data += data; // adjust tot_edge_data
    }

    // accumulate node_data: u + v -> u'
    double data_v = get(vertex_weight_t(),G,vertex_v);
    double ndata = data_v+get(vertex_weight_t(),G,vertex_u);
    boost::put(vertex_weight_t(),G,vertex_u,ndata);
    tot_node_data += data_v;

    // edges under u
    double edata = get(edges_under_t(),G,vertex_v)+get(edges_under_t(),G,vertex_u)+edge_data_uv;
    boost::put(edges_under_t(),G,vertex_u,edata);

#ifdef DEBUG
    // children
    children_vec_t& children_under_u = *get(children_under_t(),G,vertex_u);
    children_vec_t& children_under_v = *get(children_under_t(),G,vertex_v);

    for(size_t jj=0; jj<children_under_v.size(); ++jj)
      children_under_u.push_back( children_under_v[jj] );
#endif

    // delete node v
    del_node(v);

#ifdef DEBUG
    assert( abs(tot_node_data - tot_node_data_org) < 1e-5 );
    if( abs(tot_edge_data + edge_data_uv - tot_edge_data_org) > 1e-5 ){
      cerr << "e(u,v)=" << edge_data_uv << endl;
      cerr << "tot_edge_data=" << tot_edge_data << endl;
      cerr << "vs " << tot_edge_data_org << endl;
    }
    assert( abs(tot_edge_data + edge_data_uv - tot_edge_data_org) < 1e-5 );
#endif
    return u;
  }

  size_t get_num_edges(){ return num_edges(*data_graph); }
  size_t get_num_nodes(){ return num_vertices(*data_graph); }
  node_t get_max_node_idx(){ return max_node_idx; }

  double get_tot_node_data(){ return tot_node_data; }
  double get_tot_edge_data(){ return tot_edge_data; }

  // debugging purpose
  node_t get_nth_node( int pos ){
    node_hash_t::iterator it;
    if( pos < 0 ) return -1;
    node_t ret=0;
    for(it=node_hash->begin(); it!=node_hash->end(); ++it){
      if( pos == 0 ) { ret=it->first; break; }
      pos--;
    }
    return ret;
  }

protected:
  // use with *caution*
  data_vertex_t get_vertex( node_t u ){
#ifdef DEBUG
    assert(has_node(u) );
#endif
    node_hash_t& hash = *node_hash;
    return hash[u];
  }

  data_edge_t get_edge( node_t u, node_t v ){
#ifdef DEBUG
    assert( has_edge(u,v) );
#endif
    node_hash_t& hash = *node_hash;
    data_edge_t edge_uv; bool ok;
    tie(edge_uv, ok) = boost::edge( hash[u], hash[v], *data_graph );
#ifdef DEBUG
    assert( ok );
#endif
    return edge_uv;
  }

public:
  ////////////////////////////////////////////////////////////////
  // declare visitor functions

  // adjacency_visitor_t of u
  //    for some x in N(u)
  // f( edge_data_ux, node_data_u, node_data_v )
  typedef boost::function< void (const double, const double, const double) > adj_data_visitor_t;

  //     // union neighbor visitor of u,v
  //     //    for some x in N(u)+N(v)
  //     // f( edge_ux, edge_vx, node_u, node_v, node_x )
  //     typedef boost::function< void (const double&, const double&, const double&, const double&, const double&)
  //                       > union_neigh_visitor_t;

  // union neighbor visitor of u,v with node_t x
  // f( x, edge_ux, edge_vx, node_u, node_v, node_x )
  typedef boost::function< void (const node_t, const double, const double, const double, const double, const double)
			   > union_neigh_data_visitor_t;

#ifdef DEBUG
  typedef boost::function< void (const double&, const double&, const double&,
				 children_vec_t&, children_vec_t&) > edge_visitor_t;
#endif
  // perhaps higher level functors ... 
  // functors taking node_t
  typedef boost::function< void (node_t, node_t) > pair_visitor_t;
  typedef boost::function< double (node_t, node_t) > pair_visitor_double_t;
  typedef boost::function< void (node_t) > node_visitor_t;

  // make visitor functions friends
  friend void visit_nodes( hac_data_network_t& hac_data,        
			   node_visitor_t& func );
    
  // O(d)
  friend void visit_immediate_pairs_single( hac_data_network_t& hac_data,
					    node_t u, pair_visitor_t& func );
  // O(V) O(d)
  friend void visit_immediate_pairs( hac_data_network_t& hac_data,
				     pair_visitor_t& func );
  // O(d) O(d)
  friend void visit_2ndorder_pairs_single( hac_data_network_t& hac_data,
					   node_t u, pair_visitor_t& func );
  // O(V) O(d) O(d)
  friend void visit_2ndorder_pairs( hac_data_network_t& hac_data,
				    pair_visitor_t& func );

  friend void visit_union_neigh_pairs( hac_data_network_t& hac_data,
				       node_t u, node_t v, pair_visitor_t& func );

  // union neighbors for all x in  N(u) + N(v)
  // usually to compute some score using data
  friend void visit_data_union_neighbors( hac_data_network_t& hac_data,
					  node_t u, node_t v,
					  union_neigh_data_visitor_t& func );
  // visit other neighbors not in union neighbors of u and v
  friend void visit_data_inv_union_neighbors( hac_data_network_t& hac_data,
					      node_t u, node_t v,
					      union_neigh_data_visitor_t& func );
#ifdef DEBUG

  // mainly for debugging purpose
  friend void visit_data_edges( hac_data_network_t& hac_data,
				edge_visitor_t& func );

  // mainly for debugging purpose
  // sum node data of a set
  friend double sum_nodes_within( hac_data_network_t::children_vec_t& set_u,    
				  hac_data_network_t& other_hac_data );

  // sum edge data between two sets on (perhaps other?) data
  friend double sum_edges_between( hac_data_network_t::children_vec_t& set_u,
				   hac_data_network_t::children_vec_t& set_v,
				   hac_data_network_t& other_hac_data );
#endif
    
  friend struct hac_traits;

}; // end of data network class



////////////////////////////////////////////////////////////////
// data network with vertex features
class vf_hac_data_network_t : public hac_data_network_t {

public:
  typedef hac_data_network_t::node_t node_t;
  typedef vector<double> feature_vec_t;
  static const bool has_feature = true;

private:
  typedef boost::shared_ptr< feature_vec_t > feature_ptr_t;
  typedef boost::unordered_map<node_t, feature_ptr_t> feature_map_t;
  feature_map_t feature_map;

public:

  vf_hac_data_network_t () : hac_data_network_t () { }
  ~vf_hac_data_network_t () { }


  int add_node( node_t u, double data ) {
    int ret = hac_data_network_t::add_node(u,data);
#ifdef DEBUG
    assert( feature_map.count(u) == 0 );
#endif
    if( ret==EXIT_SUCCESS ) 
      feature_map[u] = feature_ptr_t( new feature_vec_t );
    return ret;
  }

  int del_node( node_t u ){
    if( feature_map.count(u) > 0 ) feature_map.erase(u);
    return hac_data_network_t::del_node(u);
  }

  // u + v -> u
  node_t contract_nodes( node_t u, node_t v ){
    size_t sz = feature_map[u]->size();
#ifdef DEBUG
    assert( sz == feature_map[v]->size() );
#endif
    for(size_t j=0; j<sz; ++j)
      feature_map[u]->at(j) += feature_map[v]->at(j);

    return hac_data_network_t::contract_nodes(u,v);
  }

  feature_vec_t& get_feature_vec( node_t u ){
#ifdef DEBUG
    assert( hac_data_network_t::has_node( u ) );
    assert( feature_map.count(u) > 0 );
#endif
    return *(feature_map[u].get());
  }

};

////////////////////////////////////////////////////////////////
// for safe pointer usage
typedef boost::shared_ptr<hac_data_network_t> hac_data_ptr_t;
typedef std::vector< hac_data_ptr_t > hac_data_ptr_vec_t;
typedef boost::shared_ptr<vf_hac_data_network_t> vf_hac_data_ptr_t;
typedef std::vector< vf_hac_data_ptr_t > vf_hac_data_ptr_vec_t;


#endif
// EOF
