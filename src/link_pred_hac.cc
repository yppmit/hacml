/////////////////////////////////////////////////////
//
// Dynamic Hierarchical Agglomerative Clustering
//
// (c) 2010-2011, Yongjin Park (ypark28@jhu.edu)
//                Joel S. Bader (joel.bader@jhu.edu)
//
//     Bader Lab, Johns Hopkins University
//
// Distributed under BOOST Software license
// see "boost_1_49_0/LICENSE_1_0.txt"
//
// Source code   : Yongjin Park
// Contributors  : Yongjin Park, Joel Bader
//
/////////////////////////////////////////////////////

#include "link_pred_hac.hh"

void print_help(){
    cerr << "\n<usage>" << endl;
    cerr << "./link_pred_hac order.merge train.pairs" << endl;
    cerr << "STDIN selected pairs" << endl;
    cerr << "STDOUT prediction results" << endl;
    cerr << endl;
    cerr << "HAC-ML (c) 2010-2011 Yongjin Park and Joel S. Bader" << endl;
    cerr << "       {ypark28, joel.bader}@jhu.edu" << endl;
    cerr << endl;
}


int main(int argc, const char* argv[]){

    if( argc < 3 ){ print_help(); return -1; }

    merge_pair_vec_t mvec;
    {
        int u,v;
        ifstream fin( argv[1], ios::in );
        while(fin >> u >> v){ 
            merge_pair_ptr_t merge_pair( new merge_pair_t(u,v) );
            mvec.push_back( merge_pair ); 
        }
        fin.close();
    }

    hac_data_ptr_t hac_data_ptr = read_upair_file<hac_data_network_t>( argv[2] );
    hac_data_network_t& Gtrain = *(hac_data_ptr.get());

    // selected pairs
    // store them in a graph
    hac_data_network_t Gtest;
    {
        int u,v;
        vector<int> vec1; vector<int> vec2;
        while(cin >> u >> v){        
            Gtest.add_node(u,0.); Gtest.add_node(v,0.);
            vec1.push_back(u); vec2.push_back(v);
        }
        
        for(size_t r=0; r<vec1.size(); ++r){
            Gtest.add_data_edge(vec1[r],vec2[r],0.);
        }
    }

    // make prediction
    predict( Gtrain, Gtest, mvec );
    
    return 1;
}
// EOF
