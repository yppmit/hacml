/////////////////////////////////////////////////////
//
// Dynamic Hierarchical Agglomerative Clustering
//
// (c) 2010-2012, Yongjin Park (ypark28@jhu.edu)
//                Joel S. Bader (joel.bader@jhu.edu)
//
//     Bader Lab, Johns Hopkins University
//
// Distributed under BOOST Software license
// see "boost_1_49_0/LICENSE_1_0.txt"
//
// Source code   : Yongjin Park
// Contributors  : Yongjin Park, Joel Bader
//
/////////////////////////////////////////////////////

#ifndef HAC_LINK_PRED_HH_
#define HAC_LINK_PRED_HH_

#include "hac.hh"

// predict pairs by implicit tree structure
// pairs are included in Gtest
// STDOUT: u <\t> v <\t> score <\n>
void predict( hac_data_network_t& G,
              hac_data_network_t& Gtest,
              merge_pair_vec_t& mvec ){

    typedef hac_data_network_t::node_t node_t;
    node_t u,v; 
    double w, tot, prob;

    typedef vector<node_t> children_vec_t;
    typedef boost::unordered_map<node_t, children_vec_t> children_map_t;
    children_map_t cmap;

    // fill in "empty" nodes if necessary
    for(size_t r=0; r<mvec.size(); ++r){
        tie(u,v) = *(mvec[r].get());
        if(! G.has_node(u) ){ G.add_node(u,0); }
        if(! G.has_node(v) ){ G.add_node(v,0); }
        if( cmap.count(u) == 0 ) cmap[u].push_back(u); 
        if( cmap.count(v) == 0 ) cmap[v].push_back(v);
    }

    // estimate probability and output
    for(size_t r=0; r<mvec.size(); ++r){
        tie(u,v) = *(mvec[r].get());

        // left and right sets
        children_vec_t& left = cmap[u];
        children_vec_t& right = cmap[v];

        w = G.get_edge_data(u,v);
        tot = G.get_node_data(u) * G.get_node_data(v);
        // with prior Beta(1,1)
        prob = (w+1.) / (tot+2.);

        // output pairs between left and right
        for(size_t i=0; i<left.size(); ++i)
            for(size_t j=0; j<right.size(); ++j)
                if( Gtest.has_edge(left[i], right[j]) )
                    cout << left[i] << "\t" << right[j] << "\t"
                         << prob << endl;

        for(int j=0; j<right.size(); ++j)    // copy down
            left.push_back( right[j] );
        cmap.erase( v );
        G.contract_nodes(u,v);
    }

}

#endif
// EOF
