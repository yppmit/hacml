/////////////////////////////////////////////////////
//
// Dynamic Hierarchical Agglomerative Clustering
//
// (c) 2010-2012, Yongjin Park (ypark28@jhu.edu)
//                Joel S. Bader (joel.bader@jhu.edu)
//
//     Bader Lab, Johns Hopkins University
//
// Distributed under BOOST Software license
// see "boost_1_49_0/LICENSE_1_0.txt"
//
// Source code   : Yongjin Park
// Contributors  : Yongjin Park, Joel Bader
//
/////////////////////////////////////////////////////

#ifndef HAC_HH_
#define HAC_HH_

#include <iostream>
#include <boost/math/special_functions/gamma.hpp>
#include <cstdlib>

using namespace std;
using namespace boost;

#include "ds.hh"
#include "alg.hh"
#include "io.hh"
#include "pair_score_mat.hh"
#include "util.hh"

// ===============================================================
typedef score_matrix_t< hac_traits::node_t, double > score_mat_t;

typedef pair<hac_traits::node_t,hac_traits::node_t> merge_pair_t;
typedef boost::shared_ptr<merge_pair_t> merge_pair_ptr_t;
typedef vector<merge_pair_ptr_t> merge_pair_vec_t;
typedef vector<double> score_vec_t;
typedef boost::shared_ptr<merge_pair_vec_t> merge_pair_vec_ptr_t;
typedef boost::shared_ptr<score_vec_t> score_vec_ptr_t;
typedef pair<score_vec_ptr_t,merge_pair_vec_ptr_t> hac_global_out_t;

hac_global_out_t run_hac_global( hac_data_ptr_vec_t& hac_data_vec,
                               bool approx );

// ===============================================================
// scores that the hac algorithm uses
inline double xlogx( double x ){
    return x <=0. ? 0. : (x * log(x));
}

inline double log_beta( double e, double h ){
#ifdef DEBUG
    assert( e >= 0. && h >= 0. );
#endif
    return lgamma(e+1.) + lgamma(h+1.) - lgamma(e+h+2.);
}

inline double binom_llik( double e, double h ){
#ifdef DEBUG
    assert( e >= 0. && h >= 0. );
#endif
    return xlogx(e)+xlogx(h)-xlogx(e+h);
}

// score to determine merging (e1,h1) + (e2,h2) -> (e1+e2, h1+h2)
double binom_llik_ratio( double e1, double h1, double e2, double h2 ){
    return binom_llik(e1+e2,h1+h2) - binom_llik(e1,h1) - binom_llik(e2,h2);
}

// Bayesian counterpart
double binom_bayes_ratio( double e1, double h1, double e2, double h2 ){
    return log_beta(e1+e2,h1+h2) - log_beta(e1,h1) - log_beta(e2,h2);
}

// left-right-center comparison in maximum likelihood
double lrc_edge_ratio( double e11, double h11, double e22, double h22,
                       double e12, double h12 ){
    double num = binom_llik(e11+e12+e22, h11+h12+h22);
    double denom = binom_llik(e11,h11)+binom_llik(e22,h22)+binom_llik(e12,h12);
    return num - denom;
}

// left-right-center comparison
//          B(e11+e22+e12, h11+h22+h12)
// lambda = --------------------------------
//          B(e11,h11) B(e22,h22) B(e12,h12)
double bayes_lrc_edge_ratio( double e11, double h11, double e22, double h22,
                             double e12, double h12 ){

    double num = log_beta( e11+e22+e12, h11+h22+h12 );
    double denom = log_beta( e11, h11 ) + log_beta( e22, h22 ) + log_beta( e12, h12 );
    return num - denom;
}

// score for vertex in ml
//             (n1+n2)^(n1+n2)
// lambda_v = -----------------
//             n1^n1 n2^n2
double lrc_vertex_ratio( double n_u, double n_v ){
#ifdef DEBUG
    assert( n_u >= 0 && n_v >= 0 );
#endif
    double num = xlogx(n_u+n_v);
    double denom = xlogx(n_u) + xlogx(n_v);
    return num - denom;
}


// score for vertex
//             Gam(a*m1)Gam(a*m2)    Gam(n1+n2+a*(m1+m2))
// lambda_v = -------------------- -------------------------
//             Gam(a*(m1+m2))      Gam(n1+a*m1) Gam(n2+a*m2)
//
// empirical choice of hyperparameters
// a = 1
// {m_i} = {n_i / Ntot}
double bayes_lrc_vertex_ratio( double n1, double n2, double ntot, double a=1. ){
#ifdef DEBUG
    assert( n1 > 0 && n2 > 0 );
#endif
    double p = a / ntot;
    double s = 1. + p;
    double num = lgamma((n1+n2)*s) + lgamma(n1*p) + lgamma(n2*p);
    double denom = lgamma((n1+n2)*p) + lgamma(n1*s) + lgamma(n2*s);
    return num - denom;
}
// ===============================================================



// ===============================================================
// functors required for clustering score computation
// compute pairwise merging score
struct func_merge_score{
    func_merge_score(double& acc) : accum(acc) { }

    void operator() ( hac_data_network_t::node_t x,
                      double ux_data, double vx_data,
                      double u_data, double v_data, double x_data ){

        double e1, h1, e2, h2;
        e1 = ux_data;
        e2 = vx_data;
        h1 = u_data*x_data - ux_data;
        h2 = v_data*x_data - vx_data;
        accum += binom_llik_ratio( e1, h1, e2, h2 );
    }

    double& accum;
};



// ================================================================
// HAC merge score
struct func_calc_merge_score{

    func_calc_merge_score( vf_hac_data_network_t& _vf,        
                           hac_data_ptr_vec_t& _hd_arr,
                           score_mat_t& _sm ) : 
        vfhd(_vf), hd_arr(_hd_arr), smat(_sm) { }
        

    typedef hac_traits::node_t node_t;

    inline double get_edge_data(node_t u, node_t v, int d){
        hac_data_network_t& hd = *hd_arr[d];
        return hd.has_edge(u,v) ? hd.get_edge_data(u,v) : 0.;
    }

    inline double get_node_data(node_t u, int d){
        hac_data_network_t& hd = *hd_arr[d];
        return hd.has_node(u) ? hd.get_node_data(u) : 0.;
    }

    inline double get_edge_under(node_t u, int d){
        hac_data_network_t& hd = *hd_arr[d];
        return hd.has_node(u) ? hd.get_edge_under(u) : 0.;
    }

    // calculate and set score
    double calc_score(node_t u, node_t v){

        double score = 0.;

        for(size_t d=0; d<hd_arr.size(); ++d){

            double n1 = get_node_data(u,d);
            double n2 = get_node_data(v,d);

            // meaningless to compute
            if( n1 == 0 || n2 == 0 ) continue;

            double e1 = get_edge_under(u,d);
            double e2 = get_edge_under(v,d);
            double e12 = get_edge_data(u,v,d);
            double h1 = n1*(n1-1)/2 - e1;
            double h2 = n2*(n2-1)/2 - e2;
            double h12 = n1*n2 - e12;

            // ================ P(G|M) ratio ================
            // score for between-edges
            double tmp_score = 0.;
            hac_traits::union_neigh_visitor_t func = func_merge_score( tmp_score );
            hac_data_network_t& hd = *hd_arr[d];
            visit_data_union_neighbors( hd, u, v, func );

            // Additional LRC score
            tmp_score += lrc_edge_ratio(e1,h1,e2,h2,e12,h12);

            // ================ P(M) ratio ================
            tmp_score += lrc_vertex_ratio(n1,n2);

            score += tmp_score;
        }

#ifdef DEBUG
        assert( vfhd.has_node(u) && vfhd.has_node(v) );
#endif

        typedef vf_hac_data_network_t::feature_vec_t feature_vec_t;
        feature_vec_t& feat1 = vfhd.get_feature_vec(u);
        feature_vec_t& feat2 = vfhd.get_feature_vec(v);
#ifdef DEBUG
        assert( feat1.size() == feat2.size() );
#endif

        double n1 = vfhd.get_node_data(u), n2 = vfhd.get_node_data(v);
        for(size_t j=0; j<feat1.size(); ++j){
            double nf1 =  feat1[j], nf2 = feat2[j];
            score += binom_llik_ratio( nf1, n1-nf1, nf2, n2-nf2 );
        }

        return score;
    }

    // record-keeping in score matrix
    void operator () (node_t u, node_t v){
        // calculate score
        double scr = calc_score(u,v);
        smat.set_score( u, v, scr );
    }

    vf_hac_data_network_t& vfhd; // vertex-featured data
    hac_data_ptr_vec_t& hd_arr;  // array of data graphs
    score_mat_t& smat;           // score matrix
};



// delta-update to save computation time
// adjusted score (k1,k2) after merging (l1,l2)
struct func_delta_score{
    func_delta_score( hac_data_ptr_vec_t& _hd_arr ) :
        hd_arr(_hd_arr) {}

    typedef hac_traits::node_t node_t;

    inline
    double get_edge_data(node_t u, node_t v, int d){
        hac_data_network_t& hd = *hd_arr[d];
        return hd.has_edge(u,v) ? hd.get_edge_data(u,v) : 0.;
    }

    inline
    double get_node_data(node_t u, int d){
        hac_data_network_t& hd = *hd_arr[d];
        return hd.has_node(u) ? hd.get_node_data(u) : 0.;
    }

    double operator()( node_t k1, node_t k2, node_t l1, node_t l2 ){
#ifdef DEBUG
        assert( k1 != k2 && l1 != l2 );
        assert( k1 != l1 && k2 != l2 );
        assert( k1 != l2 && k2 != l1 );
#endif
        double ret=0.;

        double edge1, edge2, hole1, hole2;
        double edge1Cum, edge2Cum, hole1Cum, hole2Cum;
        double nK1, nK2, nL1, nL2;

        for(size_t d=0; d<hd_arr.size(); ++d){

            nK1 = get_node_data(k1,d); nK2 = get_node_data(k2,d);

            if( nK1 == 0 || nK2 == 0 ) continue;

            nL1 = get_node_data(l1,d); nL2 = get_node_data(l2,d);

            // old score
            double score_old = 0.;
            // I(a) score of k1, k2 toward l1
            edge1 = get_edge_data(k1,l1,d); edge2 = get_edge_data(k2,l1,d);
            hole1 = nK1*nL1 - edge1; hole2 = nK2*nL1 - edge2;
            score_old += binom_llik_ratio( edge1, hole1, edge2, hole2 );
            edge1Cum = edge1; edge2Cum = edge2;
            hole1Cum = hole1; hole2Cum = hole2;

            // I(b) score of k1, k2 toward l2
            edge1 = get_edge_data(k1,l2,d); edge2 = get_edge_data(k2,l2,d);
            hole1 = nK1*nL2 - edge1; hole2 = nK2*nL2 - edge2;
            score_old += binom_llik_ratio( edge1, hole1, edge2, hole2 );
            edge1Cum += edge1; edge2Cum += edge2;
            hole1Cum += hole1; hole2Cum += hole2;

            // new score
            // II. assuming l1 and l2 joined
            double score_new
                = binom_llik_ratio( edge1Cum, hole1Cum, edge2Cum, hole2Cum );

            ret += (score_new - score_old);
        }
        return ret;
    }

    hac_data_ptr_vec_t& hd_arr;
};

// collect pairs that need a delta-update
// forall x in N(u)+N(v)
//  forall y, s.t (x,y) in smat
//   y in N(x) in 1st order candidate search
//   y in N2(x) in 2nd order candidate
struct func_delta_update_search {

    typedef hac_traits::node_t node_t;
    typedef boost::unordered_set<node_t> vset_t;
    typedef boost::unordered_map<node_t, vset_t> candidate_map_t;

    func_delta_update_search(hac_data_network_t& _hd,
            candidate_map_t& _c, bool _approx) : hd(_hd), candidate(_c) {
        approx = _approx;
    }

    void set_merge_pair(node_t u, node_t v){
        taboo_u = u; taboo_v = v; candidate.clear();
    }

    void operator()(node_t u, node_t x){
        hac_traits::pair_visitor_t f = fsub(*this);
        if( approx ){ visit_immediate_pairs_single(hd,x,f); }
        else { visit_2ndorder_pairs_single(hd,x,f); }
    }

    hac_data_network_t& hd;
    candidate_map_t& candidate;

    node_t taboo_u, taboo_v;
    bool approx;

    ~func_delta_update_search(){}

    struct fsub {

        fsub(func_delta_update_search& f) : F(f) {}

        void operator()(node_t _x, node_t _y){
            node_t x = _x, y = _y;
            if( _x > _y ){ x = _y; y = _x; }
            if( x == y || x == F.taboo_u || x == F.taboo_v ||
                    y == F.taboo_u || y == F.taboo_v ) return;

            candidate_map_t& cmap = F.candidate;
            cmap[x].insert(y);
        }

        func_delta_update_search& F;
    };
};

// ================================================================
// collapsing by model comparison

// Bayesian between Pr(e_{ij}) comparison
struct func_bayes_between_edge{

    func_bayes_between_edge(double& _a) : accum(_a) {}
    void operator()( hac_data_network_t::node_t x,
                     double e_ux, double e_vx,
                     double n_u, double n_v, double n_x ){
        double h_ux = n_u*n_x - e_ux;
        double h_vx = n_v*n_x - e_vx;
        accum += binom_bayes_ratio( e_ux, h_ux, e_vx, h_vx );
    }
    double& accum;
};

// Bayesian between Pr(e_{ij}) comparison,
// a faster version for null edge
struct func_bayes_between_edge_null{

    func_bayes_between_edge_null(double& _a): accum(_a){}
    void operator()( hac_data_network_t::node_t x,
                     double e_ux, double e_vx,
                     double n_u, double n_v, double n_x ){
        //  (n1*n_k+1) (n2*n_k+1)
        // ----------------------
        //  ((n1+n2)*n_k+1)
        accum += log(n_u*n_x+1)+log(n_v*n_x+1)-log((n_u+n_v)*n_x+1);
    }
    double& accum;
};

// ================================================
// bayesian model comparison score

struct func_bayes_collapse_score{

    typedef hac_traits::node_t node_t;

    func_bayes_collapse_score( vf_hac_data_network_t& _vf,
                               hac_data_ptr_vec_t& _hd_arr ) :
        vfhd(_vf), hd_arr(_hd_arr) {}

    inline double get_edge_data(node_t u, node_t v, int d){
        hac_data_network_t& hd = *hd_arr[d];
        return hd.has_edge(u,v) ? hd.get_edge_data(u,v) : 0.;
    }

    inline double get_node_data(node_t u, int d){
        hac_data_network_t& hd = *hd_arr[d];
        return hd.has_node(u) ? hd.get_node_data(u) : 0.;
    }

    inline double get_edge_under(node_t u, int d){
        hac_data_network_t& hd = *hd_arr[d];
        return hd.has_node(u) ? hd.get_edge_under(u) : 0.;
    }


    double operator() (node_t u, node_t v){

        double ret= 0.;

        for(size_t d=0; d<hd_arr.size(); ++d){

            double n1 = get_node_data(u,d);
            double n2 = get_node_data(v,d);
            double tmp_score = 0.;

            if( n1 == 0 || n2 == 0 ) continue;

            double e1 = get_edge_under(u,d);
            double e2 = get_edge_under(v,d);
            double e12 = get_edge_data(u,v,d);
            double h1 = n1*(n1-1)/2 - e1;
            double h2 = n2*(n2-1)/2 - e2;
            double h12 = n1*n2 - e12;

            // comparison of Pr(within edge) by LRC comparison
            // bayes_collapse_var >= 1
            tmp_score += bayes_lrc_edge_ratio(e1,h1,e2,h2,e12,h12);

            // comparison of Pr(between edge)
            hac_data_network_t& hd = *hd_arr[d];

            double ret_bet = 0.;
            hac_traits::union_neigh_visitor_t
            func = func_bayes_between_edge(ret_bet);
            visit_data_union_neighbors( hd, u, v, func );

            // *note* this could be a major source of clutter
            //double ret_bet0 = 0.;
            //hac_traits::union_neigh_visitor_t
            //    func0 = func_bayes_between_edge_null(ret_bet);
            //visit_data_inv_union_neighbors( hd, u, v, func0 );
            //ret_bet += ret_bet0;

            tmp_score += ret_bet;

            ret += tmp_score;
        }

#ifdef DEBUG
        assert( vfhd.has_node(u) && vfhd.has_node(v) );
#endif

        typedef vf_hac_data_network_t::feature_vec_t feature_vec_t;
        feature_vec_t& feat1 = vfhd.get_feature_vec(u);
        feature_vec_t& feat2 = vfhd.get_feature_vec(v);
#ifdef DEBUG
        assert( feat1.size() == feat2.size() );
#endif
        double n1 = vfhd.get_node_data(u), n2 = vfhd.get_node_data(v);
        for(size_t j=0; j<feat1.size(); ++j){
            double nf1 =  feat1[j], nf2 = feat2[j];
            ret += binom_llik_ratio( nf1, n1-nf1, nf2, n2-nf2 );
        }

        return ret;
    }

    vf_hac_data_network_t& vfhd;
    hac_data_ptr_vec_t& hd_arr;
};


// ================================================
// fill in discrepancy
struct func_fill_zero_node{
    typedef hac_traits::node_t node_t;

    func_fill_zero_node(hac_data_ptr_vec_t& _hd_vec) : hd_vec(_hd_vec) { }

    void operator() (node_t u){

        for(size_t d=0; d<hd_vec.size(); ++d){
            hac_data_network_t& hd = *(hd_vec[d].get());
            if( !hd.has_node(u) )
                hd.add_node(u,0); // zero node data
        }
    }

    hac_data_ptr_vec_t& hd_vec;
};

// fill in edge discrepancy
// pairs in some data -> rep_data
// use: visit_immediate_pairs
struct func_fill_zero_edge{
    typedef hac_traits::node_t node_t;

    func_fill_zero_edge(vf_hac_data_network_t& _uniG): uniG(_uniG) {}

    void operator() (node_t u, node_t v){
        if(!uniG.has_node(u)){ uniG.add_node(u,1); }
        if(!uniG.has_node(v)){ uniG.add_node(v,1); }
        if(!uniG.has_edge(u,v)){ uniG.add_data_edge(u,v,0); }
    }

    vf_hac_data_network_t& uniG;
};


// ================================================================================
// union data network that can hold vertex features
vf_hac_data_ptr_t mk_union_data( hac_data_ptr_vec_t& hac_data_vec ){

    // create representative data aggregating all
    // to determine candidate sets
    vf_hac_data_ptr_t out( new vf_hac_data_network_t() );
    vf_hac_data_network_t& G = *(out.get());

    // add pseudo nodes and edges to rep_data
    // to figure out total dependency
    // generously include all edges
    for(size_t d=0; d<hac_data_vec.size(); ++d){
        hac_data_network_t& hd = *hac_data_vec[d];
        hac_traits::pair_visitor_t filler = func_fill_zero_edge(G);
        visit_immediate_pairs(hd, filler);
    }
    // fill in discrepancy across different graphs
    {
        hac_traits::node_visitor_t filler =
            func_fill_zero_node(hac_data_vec);
        visit_nodes( G, filler );
    }

    return out;
}


// should also take vertex-feature vector graph
hac_global_out_t run_hac_global( vf_hac_data_ptr_t rep_data_ptr,  // union net
                                 hac_data_ptr_vec_t& hac_data_vec,  // data net vectors
                                 bool approx=false ){               // approximation


    // ================ prepare D/S ================
    score_mat_t* score_mat = new score_mat_t(); // score mat to get max pair
    vf_hac_data_network_t* rep_data = rep_data_ptr.get();

    score_vec_ptr_t score_vec_ptr( new score_vec_t );          // score vector pointer
    score_vec_t& cum_score_vec = *(score_vec_ptr.get());       // cumulative score vector
    merge_pair_vec_ptr_t merge_vec_ptr( new merge_pair_vec_t );// merge order pointer
    merge_pair_vec_t& merge_vec = *(merge_vec_ptr.get());      // merge order vector

    hac_global_out_t out( score_vec_ptr, merge_vec_ptr );      // output

    hac_traits::pair_visitor_t calc_pair =
        func_calc_merge_score( *rep_data, hac_data_vec, *score_mat );

#ifdef DEBUG
    func_calc_merge_score
        calc_pair_debug( *rep_data, hac_data_vec, *score_mat );  // debug score
#endif

    // delta score updater
    func_delta_score fdelta( hac_data_vec );

#ifdef EXPERIMENTAL
    func_delta_update_search::candidate_map_t candidate;
    func_delta_update_search fdsearch( *rep_data, candidate, approx );
#endif

    // collapsing score to determine resolution
    hac_traits::pair_visitor_double_t collapse_score
        = func_bayes_collapse_score( *rep_data, hac_data_vec );

    // ================ initial computation ================
    //
    int iter=0;
//     cerr << "Input graphs:\n";
//     for(size_t d=0; d<hac_data_vec.size(); ++d){
//         cerr << "V[" << d << "]=" << hac_data_vec[d]->get_tot_node_data();
//         cerr << " E[" << d << "]=" << hac_data_vec[d]->get_tot_edge_data();
//         cerr << endl;
//     }
//     cerr << endl;

    if( approx ){                 // immediately connected pairs
        visit_immediate_pairs( *rep_data, calc_pair );
    }else{                            // with neighbors of neighbors
        visit_2ndorder_pairs( *rep_data, calc_pair );
    }

    // get score matrix iterator
    score_mat_t::pair_iterator_t* smat_it = score_mat->new_iterator();

    cerr << "\n #Candidate Pairs=" << score_mat->size() << endl;


    // ================ main loop ================
    double cum_score = 0.;
    cum_score_vec.push_back( cum_score );

    score_mat_t::ps_t* best_score;
    hac_traits::node_t u, v, x, y, w;

    while( score_mat->size() > 0 ){

        best_score = score_mat->find_max_score();
        u = best_score->key1; v = best_score->key2;

        ++iter;
#ifdef DEBUG
        double best_sc = (best_score->score);
        cerr.precision(2);
        cerr << u << " - " << v << " score ";
        cerr << fixed << best_sc;
#else
        if( (iter%10)==0 ) { cerr <<"."; cerr << flush; }
        if( (iter%500)==0 ) { cerr << ". |score_matrix|=" << score_mat->size() << endl; }
#endif

        double curr_c_score = collapse_score(u,v);    // collapsing score
        cum_score += curr_c_score;                    // accum Bayes factors
        cum_score_vec.push_back( cum_score );         // record-keeping
        merge_pair_ptr_t merge_pair( new merge_pair_t(u,v) );
        merge_vec.push_back( merge_pair );

#ifdef DEBUG                                          //
        cerr << ", collapse score= " << curr_c_score; // current delta log Pr
        cerr << " K=" << rep_data->get_num_nodes();   // # clusters
        cerr << " cum_score= " << cum_score << endl;  // cum delta log Pr(G|M)
#endif                                                //
                                                      // clean-up score_mat
        score_mat->del_scores_with(u);                // u,v -> u'
        score_mat->del_scores_with(v);                // collapsed

        // ================ delta-update ================
        // any score neighboring either u or v, or both
        if( rep_data->get_num_nodes() > 4 && score_mat->size() > 0 ){

#ifdef EXPERIMENTAL
            fdsearch.set_merge_pair(u,v);
            hac_traits::pair_visitor_t ftmp = fdsearch;
            visit_immediate_pairs_single(*rep_data,u,ftmp);
            visit_immediate_pairs_single(*rep_data,v,ftmp);

            func_delta_update_search::candidate_map_t::const_iterator it;
            for(it=candidate.cbegin(); it!=candidate.cend(); ++it){
                x = it->first;
                func_delta_update_search::vset_t::const_iterator jt;
                for(jt=(it->second).cbegin(); jt!=(it->second).cend(); ++jt){
                    y = *jt;
                    if( score_mat->has_score(x,y) ){
                        score_mat_t::ps_t* sobj = score_mat->get_score(x,y);
                        double old_score = sobj->score;            // old score
                        double delta_score = fdelta(x,y,u,v);      // update score
                        score_mat->set_score(x, y, (old_score+delta_score));
                    }
                }
            }

#else
            for( smat_it->set_begin(); !smat_it->is_end(); smat_it->advance() ){
                score_mat_t::ps_t* sobj = smat_it->get_data(); // find anything
                x = sobj->key1; y = sobj->key2;                // being affected

                if( rep_data->has_edge(u,x) || rep_data->has_edge(v,x)
                    || rep_data->has_edge(u,y) || rep_data->has_edge(v,y) ){

                    double old_score = sobj->score;            // old score
                    double delta_score = fdelta(x,y,u,v);      // update score
                    score_mat->set_score(x, y, (old_score+delta_score));
                }
            }
#endif
            // *note* order matters u + v -> u
            w = rep_data->contract_nodes(u,v);           // actually merge node
            for(size_t d=0; d<hac_data_vec.size(); ++d){//
                hac_data_ptr_t Gptr = hac_data_vec[d];
                Gptr->contract_nodes(u,v);
            }            

#ifdef DEBUG                                             // check delta-update
            {                                            // with brute-force comp.
                double sto_score, debug_score;           //
                size_t ntest = 0;                        // # of tests/validation
                for( smat_it->set_begin();               // Within score matrix
                     !smat_it->is_end();                 // iterate affected ones
                     smat_it->advance() ){               //
                    score_mat_t::ps_t* sobj = smat_it->get_data();
                    sto_score = sobj->score;             // stored score
                    x = sobj->key1; y = sobj->key2;      // vs debug score
                    if( rep_data->has_edge(w,x) || rep_data->has_edge(w,y) ){
                        debug_score = calc_pair_debug.calc_score(x,y);

                        if( abs(sto_score-debug_score) > 1e-5 ){
                            cerr << "error: ";
                            cerr << "stored = " << sto_score << " vs "
                                 << "debug = " << debug_score
                                 << endl;
                            assert(false);
                        }                                // erorr message
                        ntest++;                         // #tests
                    }                                    //
                }                                        // end of score mat iter
                cerr << "num test = " << ntest << endl;  //
            }                                            //
#endif

            // ================ new pairs involving w ================
            if( approx ){
                visit_immediate_pairs_single( *rep_data, w, calc_pair );
            }else{
                visit_2ndorder_pairs_single( *rep_data, w, calc_pair );
            }

        }else{
            // not doing delta update... small data
            // *note* order matters: merge u and v -> u
            w = rep_data->contract_nodes(u,v);
            for(size_t d=0; d<hac_data_vec.size(); ++d){
                hac_data_ptr_t Gptr = hac_data_vec[d];
                Gptr->contract_nodes(u,v);
            }

            score_mat->empty_mat();              // recompute everything
            if( approx ){
                visit_immediate_pairs( *rep_data, calc_pair );
            }else{
                visit_2ndorder_pairs( *rep_data, calc_pair );
            }
        }

    } // ================ end of main loop ================

    int num_clusters = rep_data->get_num_nodes();
    if(iter>=100 && iter%1000!=0) cerr << endl;
    cerr << "DONE K=" << num_clusters << "\n" << endl;

    delete smat_it;
    delete score_mat;

    return out;
}


#endif
// EOF
