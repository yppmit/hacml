/////////////////////////////////////////////////////
//
// Hierarchical Agglomerative Clustering
//
// (c) 2010, Yongjin Park (ypark28@jhu.edu)
//           Joel S. Bader (joel.bader@jhu.edu)
//
//     Bader Lab, Johns Hopkins University
//
// Distributed under BSD license
// See the attached "bsd.txt"
//
// Source code   : Yongjin Park
// Contributors  : Yongjin Park, Joel Bader
//
/////////////////////////////////////////////////////

#ifndef HAC_SAMPLE_HH_
#define HAC_SAMPLE_HH_

#include"ds.hh"
#include"io.hh"
#include"util.hh"

#include<iostream>
#include<fstream>
#include<sstream>
#include<string>
#include<cassert>
#include<cmath>
#include<cstdlib>
#include<vector>
#include<algorithm>
#include<cmath>
#include<ctime>
#include<boost/unordered_map.hpp>
#include<boost/unordered_set.hpp>

using namespace std;
using namespace boost;

typedef pair<int,int> test_pair_t;
typedef boost::shared_ptr<test_pair_t> test_pair_ptr_t;
typedef vector<test_pair_ptr_t> test_pair_vec_t;

boost::shared_ptr<test_pair_vec_t>
make_test_set( string net_file, double pp, int seed );

boost::shared_ptr<test_pair_vec_t>
select_test_set( hac_data_ptr_t, double pp, int seed );

// Random sampling
// random index [0 .. maxval)
int rand_idx( int maxval ) {
    return rand() % maxval;
}


///////////////////////////////////
// generate test/train networks
//
boost::shared_ptr<test_pair_vec_t>
make_test_set( string net_file, double pp, int seed ){

    hac_data_ptr_t G = read_upair_file<hac_data_network_t>( net_file.c_str() );
    double etot = G->get_num_edges();
    double ntot = G->get_num_nodes();
    hac_traits::node_t nmax = G->get_max_node_idx();

    // test & train output
    string tmp = lexical_cast<string>( (int) 100*pp );
    string test_out_file = get_fname( net_file ) + "-p" + tmp + "-test.pairs";
    string train_out_file = get_fname( net_file ) + "-p" + tmp + ".pairs";

    vector<int> vlist1, vlist2;

    for(int i=1; i<=nmax; ++i){
        if(G->has_node(i)) {
            vlist1.push_back(i);
            vlist2.push_back(i);
        }
    }

    srand(seed);

    int htot = (ntot*(ntot-1)/2.) - etot;

    int edge_max = lexical_cast<int>( ceil(etot * pp) );
    int hole_max = lexical_cast<int>( ceil(htot * pp) );
    int tot_max = edge_max + hole_max;
    int ee=0, hh=0;
#ifdef DEBUG
    cerr << "E=" << edge_max << endl;
    cerr << "H=" << hole_max << endl;
#endif

    boost::shared_ptr<test_pair_vec_t> test_pair_ptr ( new test_pair_vec_t );
    ofstream test_out( test_out_file.c_str() );
    int u,v;
    // randomly select pairs until we meet requirement
    // it might include redundant pairs in the test-set
    // check redundancy with hash set

    // key: u*nmax + v where u<v
    unordered_set<unsigned long> test_hash;
    unsigned long ekey;

    while( (ee+hh) < tot_max ){

        // shuffle vertices
        random_shuffle( vlist1.begin(), vlist1.end() );
        random_shuffle( vlist2.begin(), vlist2.end() );

        for(int ui=0; ui<ntot; ++ui){
            if( (ee+hh) >= tot_max ) break;    // enough of test pairs
            u = vlist1[ui]; v = vlist2[ui];    // (u,v)
            if ( u <=0 || v<=0 ) continue;     // only natural numbers
            if ( u == v ) continue;            // no self-loop
            ekey = u < v ? u*nmax+v : v*nmax+u;// to check test set
            if( test_hash.count(ekey) ) continue;
            test_hash.insert(ekey);

            if ( G->has_edge(u,v) ){            // Select an edge of test-set
                if ( ee < edge_max ) {          // if EE did not exceed MM
                    ee++;                       // - update counter
                    G->del_edge(u,v);           // - missing link in train set
                    test_out << u << "\t" << v << "\t" << 1 << endl;
                }
            }else{
                if ( hh < hole_max ) {
                    hh ++;                      // select a hole of test-set
                    test_out << u << "\t" << v << "\t" << 0 << endl;
                }
            }
            test_pair_ptr_t curr_pair( new test_pair_t(u,v) );
            test_pair_ptr->push_back( curr_pair );
        }
    } // End of while
    write_upair_file( G, train_out_file.c_str() ); // output partial network
    return test_pair_ptr;
}

boost::shared_ptr<test_pair_vec_t>
select_test_set( hac_data_ptr_t G, double pp, int seed ){

    double etot = G->get_num_edges();
    double ntot = G->get_num_nodes();
    hac_traits::node_t nmax = G->get_max_node_idx();

    vector<int> vlist1, vlist2;

    for(int i=1; i<=nmax; ++i){
        if(G->has_node(i)) {
            vlist1.push_back(i);
            vlist2.push_back(i);
        }
    }

    srand(seed);

    int htot = (ntot*(ntot-1)/2.) - etot;

    int edge_max = lexical_cast<int>( ceil(etot * pp) );
    int hole_max = lexical_cast<int>( ceil(htot * pp) );
    int tot_max = edge_max + hole_max;
    int ee=0, hh=0;
#ifdef DEBUG
    cerr << "E=" << edge_max << endl;
    cerr << "H=" << hole_max << endl;
#endif

    boost::shared_ptr<test_pair_vec_t> test_pair_ptr ( new test_pair_vec_t );
    int u,v;
    // randomly select pairs until we meet requirement
    // it might include redundant pairs in the test-set
    // check redundancy with hash set

    // key: u*nmax + v where u<v
    unordered_set<unsigned long> test_hash;
    unsigned long ekey;

    while( (ee+hh) < tot_max ){

        // shuffle vertices
        random_shuffle( vlist1.begin(), vlist1.end() );
        random_shuffle( vlist2.begin(), vlist2.end() );

        for(int ui=0; ui<ntot; ++ui){
            if( (ee+hh) >= tot_max ) break;    // enough of test pairs
            u = vlist1[ui]; v = vlist2[ui];    // (u,v)
            if ( u <=0 || v<=0 ) continue;     // only natural numbers
            if ( u == v ) continue;            // no self-loop
            ekey = u < v ? u*nmax+v : v*nmax+u;// to check test set
            if( test_hash.count(ekey) ) continue;
            test_hash.insert(ekey);

            if ( G->has_edge(u,v) ){            // Select an edge of test-set
                if ( ee < edge_max ) { ee++; }  // if EE did not exceed MM
            }else{
                if ( hh < hole_max ) { hh++; }
            }
            test_pair_ptr_t curr_pair( new test_pair_t(u,v) );
            test_pair_ptr->push_back( curr_pair );
        }
    } // End of while
    return test_pair_ptr;
}

#endif /* SAMPLE_HH_ */
