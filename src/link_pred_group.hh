//
// link_pred_group.hh
// (c) Yongjin Park, Mar 26, 2012
//


#ifndef LINK_PRED_GROUP_HH_
#define LINK_PRED_GROUP_HH_
#include"ds.hh"
#include"io.hh"
#include"util.hh"

#include<iostream>
#include<fstream>
#include<sstream>
#include<string>
#include<cassert>
#include<cmath>
#include<vector>
#include<cmath>
#include<boost/unordered_map.hpp>

using namespace std;
using namespace boost;

void make_link_group( string train_file, string group_file, string test_file );

using namespace std;


///////////////////////////////////
// link prediction given group structure
// on testing pairs
void make_link_group( hac_data_network_t& Gtrain, 
                      hac_data_network_t& Gtest, 
                      const char* group_file ){

    // map of groups
    unordered_map< int, boost::shared_ptr< vector<int> > > groups;
    // read .group file
    ifstream grp_in(group_file, ios::in);
    string line;
    int k = -1;
    while( getline(grp_in,line,'\n') ){
        istringstream iss(line);
        string word; ++k;
        boost::shared_ptr<vector<int> > grp( new vector<int> );
        groups[k] = grp;
#ifdef DEBUG
        cerr << "k" << k;
#endif
        int u;
        while( iss >> word ){
            try{
                u = lexical_cast<int>(word);
                grp->push_back(u);
#ifdef DEBUG
                cerr << "\t" << u;
#endif
            }catch(char* msg){
                cerr << "bad: " << word << endl;
            }
        }
#ifdef DEBUG
        cerr << endl;
#endif
    }
    grp_in.close();
    int Kmax = k;

    // estimate probability and write out
    int u,v;
    for(int k=0; k<=Kmax; ++k){
        vector<int>* grp_k_ptr = groups[k].get();
        vector<int>& grp_k = *grp_k_ptr;
        // within group k-k
        {
            vector<int> u_vec; vector<int> v_vec;
            double edge=0., tot=0.;
            for(int ui=0; ui<grp_k.size(); ++ui){
                u = grp_k[ui];
                if( ! Gtrain.has_node(u) ){ continue; }
                for(int vi=ui+1; vi<grp_k.size(); ++vi){
                    v = grp_k[vi];
                    if( ! Gtrain.has_node(v) ){ continue; }
                    edge += Gtrain.has_edge(u,v)? 1 : 0.;
                    tot ++;
                    // not to create too large link file, change from:
                    // Gtest.has_node(u) && Gtest.has_node(v)
                    if( Gtest.has_edge(u,v) || Gtest.has_edge(v,u) ){
                        u_vec.push_back(u); v_vec.push_back(v);
                    }
                }
            }
            double prob = (edge+1.)/(tot+2.);
            for(int ii=0; ii<u_vec.size(); ++ii){
                cout << u_vec[ii] << "\t" << v_vec[ii];
                cout << "\t" << prob << endl;
            }
        }

        // between groups k-l
        for(int l=(k+1); l<=Kmax; ++l){
            vector<int>* grp_l_ptr = groups[l].get();
            vector<int>& grp_l = *grp_l_ptr;
            vector<int> u_vec; vector<int> v_vec;
            double edge=0., tot=0.;
            for(int ui=0; ui<grp_k.size(); ++ui){
                u = grp_k[ui];
                if( ! Gtrain.has_node(u) ){ continue; }
                for(int vi=0; vi<grp_l.size(); ++vi){
                    v = grp_l[vi];
                    if( ! Gtrain.has_node(v) ){ continue; }
                    edge += Gtrain.has_edge(u,v)? 1 : 0.;
                    tot ++;

                    // not to create too large link file, change from:
                    // if( Gtest.has_node(u) && Gtest.has_node(v) ){
                    if( Gtest.has_edge(u,v) || Gtest.has_edge(v,u) ){
                        u_vec.push_back(u); v_vec.push_back(v);
                    }
                }
            }
            double prob = (edge+1.)/(tot+2.);
            for(int ii=0; ii<u_vec.size(); ++ii){
                cout << u_vec[ii] << "\t" << v_vec[ii];
                cout << "\t" << prob << endl;
            }
        }
    }

    return;
}

#endif /* LINK_PRED_GROUP_HH_ */
