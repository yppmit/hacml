/////////////////////////////////////////////////////
//
// Dynamic Hierarchical Agglomerative Clustering
//
// (c) 2010-2012, Yongjin Park (ypark28@jhu.edu)
//                Joel S. Bader (joel.bader@jhu.edu)
//
//     Bader Lab, Johns Hopkins University
//
// Distributed under BOOST Software license
// see "boost_1_49_0/LICENSE_1_0.txt"
//
// Source code   : Yongjin Park
// Contributors  : Yongjin Park, Joel Bader
//
/////////////////////////////////////////////////////

#ifndef PAIR_SCORE_MAT_HH
#define PAIR_SCORE_MAT_HH

#include <boost/config.hpp>
#include <boost/limits.hpp>

#include <set>
#include <boost/unordered_map.hpp>

#ifndef SORT_PAIR
#define SORT_PAIR(u,v) {                        \
        if(u>v){ int tmp_uv=u; u=v; v=tmp_uv; } \
    }
#endif


template<typename key_t, typename value_t>
struct pair_score_t{
    key_t key1, key2;
    value_t score;
    pair_score_t(key_t k1, key_t k2, value_t sc){
        key1 = k1; key2 = k2; score = sc; age = 0;
    }
    int age; // to avoid redundant update
};

template<typename key_t, typename value_t>
struct pair_score_cmp_t{

    typedef pair_score_t<key_t,value_t> ps_t;

    bool operator() (ps_t* ps1, ps_t* ps2){

        if( (ps1->score) == (ps2->score) ){
            if( (ps1->key1) == (ps2->key1) ) 
                return (ps1->key2) < (ps2->key2);
            return (ps1->key1) < (ps2->key1);
        }            
        return (ps1->score) < (ps2->score);
    }
};


//////////////////////////////////////////////////////////////////
// definition of a score matrix class (row-based)

template<typename key_t, typename value_t>
class score_matrix_t {
    
public:
    typedef pair_score_t<key_t,value_t> ps_t;
    typedef pair_score_cmp_t<key_t,value_t> ps_cmp_t;

    typedef std::set<ps_t*,ps_cmp_t> score_heap_t;
    typedef typename score_heap_t::iterator heap_it ;
    
    // a pairwise map and row-set map
    typedef unordered_map<key_t, ps_t*> ps_map_t;
    typedef unordered_map<key_t, score_heap_t*> score_heap_map_t;
    typedef unordered_map<key_t, ps_map_t*> ps_map_map_t;
    
    typedef typename ps_map_t::iterator ps_map_it;
    typedef typename ps_map_map_t::iterator ps_map_map_it;

private:
    ps_map_map_t* pair_map;      // DS #1: map[id1][id2] -> edge_score
    score_heap_map_t* row_map;   // DS #2: map[id1] -> a row set of scores  
    score_heap_t* global_heap;   // DS #3: contains each row-max
    
    unsigned long num_pairs;     // number of all non-null pairs
    key_t min_id;                // min and max id
    key_t max_id;                // 
    
    void init();
    void clean();
    void add_row(key_t);
    ps_map_t* get_row(key_t);
    ps_t* get_row_max(key_t);
    void del_row(key_t);
    bool has_row(key_t);
    
    ps_t* get_max_heap(score_heap_t*);
    // void sorted_pair(key_t*,int*);
    
public:
    score_matrix_t();
    ~score_matrix_t();
    
    // pair score
    bool has_score( key_t k1, key_t k2 );
    void set_score( key_t k1, key_t k2, value_t sc );
    void del_score( key_t k1, key_t k2 );
    void del_scores_with( key_t k );
    ps_t* get_score( key_t k1, key_t k2 );
    
    ps_t* find_max_score();
    
    // misc
    unsigned long size();
    void empty_mat();

public:
    // all pair iterator
    // iterator, basically move around pair_map
    class pair_iterator_t{
    private:
        typedef typename ps_map_map_t::const_iterator row_it;
        typedef typename ps_map_t::const_iterator col_it;

    private:
        row_it CurRow;        // Row iterator
        row_it EndRow;
        col_it CurCol;        // Column iterator
        col_it EndCol;        // End of current Row
        bool IsEndIt;          // Can't go further
        ps_map_map_t* PairMap;  // Pt to PairMap
        // Get start column of current row
        col_it CurRowBegin(){ return CurRow->second->begin(); }
        // Get end column of current row
        col_it CurRowEnd(){ return CurRow->second->end(); }
    public:
        pair_iterator_t(ps_map_map_t* pmap){
            PairMap = pmap;
        }
        ~pair_iterator_t(){ }

        int set_begin(){
            // Start at new position
            CurRow = PairMap->begin();
            EndRow = PairMap->end();
            if(CurRow == EndRow){ 
                IsEndIt = true; 
                ErrMsgNL("Empty PairMap"); 
                return EXIT_FAILURE;
            }
            CurCol = CurRowBegin();
            EndCol = CurRowEnd();
            if(CurCol==EndCol){ 
                ErrMsgNL("Invalid PairMap"); 
                return EXIT_FAILURE;
            }
            IsEndIt = false;
            return EXIT_SUCCESS;
        }
        bool is_end(){ return IsEndIt; }
        void advance(){                 // Note range: [ begin .. End )
            if( CurRow == EndRow ){ IsEndIt = true; return; } // just end
            CurCol++;                   // First, advance column
            if( CurCol == EndCol ){     // What if done with this row
                ++CurRow;               // check out next row
                if( CurRow == EndRow ){ IsEndIt=true; return; } // Can't go further
                CurCol = CurRowBegin(); EndCol = CurRowEnd();
            }
        }
        // Data and index access
        key_t get_row_id(){ return CurRow->first; }
        key_t get_col_id(){ return CurCol->first; }
        ps_t* get_data(){ return CurCol->second; }
    };

    // access iterator
    pair_iterator_t* new_iterator(){
#ifdef DEBUG
        assert( size() > 0 );
#endif
        return new pair_iterator_t( pair_map );
    }

};
  
  
//////////////////////////////////
// initialize and clean-up
template<typename key_t, typename value_t>
void score_matrix_t<key_t,value_t>::init(){
    pair_map = new ps_map_map_t();       // map of map
    row_map = new score_heap_map_t();        // map of set
    global_heap = new score_heap_t(); // set
    // To make sure
    pair_map->clear();
    row_map->clear();
    global_heap->clear();
    num_pairs = 0;
    min_id = numeric_limits<value_t>::max();
    max_id = numeric_limits<value_t>::min();
}

template<typename key_t, typename value_t>
void score_matrix_t<key_t,value_t>::clean(){
    // clean up pair_map and row_map
    if (size() > 0) empty_mat();
    delete row_map;
    delete pair_map;
    delete global_heap;
}

template<typename key_t, typename value_t>
score_matrix_t<key_t,value_t>::score_matrix_t(){ init(); }

template<typename key_t, typename value_t>
score_matrix_t<key_t,value_t>::~score_matrix_t(){ clean(); }


//////////////////////////////////
// Dealing with rows
template<typename key_t, typename value_t>
void score_matrix_t<key_t,value_t>::add_row(key_t k){  
#ifdef DEBUG
    assert(! has_row(k) );
#endif
    (*pair_map)[k] = new ps_map_t();
    (*row_map)[k] = new score_heap_t();
    
    // min and max
    min_id = min( min_id, k );
    max_id = max( max_id, k );
  }

template<typename key_t, typename value_t>
bool score_matrix_t<key_t,value_t>::has_row(key_t k){
    return ( pair_map->count(k) > 0 );
}

template<typename key_t, typename value_t>
void score_matrix_t<key_t,value_t>::del_row(key_t k){
#ifdef DEBUG
    assert(has_row(k));
#endif
    ps_map_t* tmp_map = (*pair_map)[k];     // PAIR MAP
    score_heap_t* tmp_heap = (*row_map)[k]; // ROW MAP  

    // update global heap
    if( tmp_heap->size() > 0 ){
        ps_t* row_max_score = get_row_max(k);
        global_heap->erase( row_max_score );
    }
    size_t delta_num_pairs = tmp_map->size();

    // update pair map
    ps_t* es ;
    for (ps_map_it it = tmp_map->begin(); it != tmp_map->end(); ){
        es = (it->second); it++;
        delete es;
    }
    tmp_heap->clear();
    tmp_map->clear();
    delete tmp_map;  
    delete tmp_heap;    
    
    pair_map->erase(k);                     // also delete index k
    row_map->erase(k);                      // row map  
    num_pairs -= delta_num_pairs;
}

template<typename key_t, typename value_t>
typename score_matrix_t<key_t,value_t>::ps_map_t* 
score_matrix_t<key_t,value_t>::get_row(key_t k){
#ifdef DEBUG
    assert(has_row(k));
#endif
    return (*pair_map)[k];
}

template<typename key_t, typename value_t>
typename score_matrix_t<key_t,value_t>::ps_t* 
score_matrix_t<key_t,value_t>::get_row_max(key_t k){
#ifdef DEBUG
    assert( has_row(k) && ((*row_map)[k]->size() >0) );
#endif
    return get_max_heap( (*row_map)[k] );
}

template<typename key_t, typename value_t>
typename score_matrix_t<key_t,value_t>::ps_t* 
score_matrix_t<key_t,value_t>::get_max_heap(score_heap_t* heap){
    return *( heap->rbegin() );
}

//////////////////////////////////
// Dealing with pairs
//

template<typename key_t, typename value_t>
bool score_matrix_t<key_t,value_t>::has_score( key_t k1, key_t k2 ){
    SORT_PAIR(k1,k2);  
    if (!has_row(k1)) return false;
    ps_map_t* row = get_row(k1);
    return ( (row->count(k2)) > 0 );
}

//
// a function to update pair score
//
template<typename key_t, typename value_t>
void score_matrix_t<key_t,value_t>::set_score( key_t k1, key_t k2, value_t sc ){ 
    
    SORT_PAIR(k1,k2);
    ps_t *es;
    score_heap_t* row_heap;
    
    if(has_row(k1)) {
        assert( ((*row_map)[k1])->size() > 0 );
        es = get_row_max(k1);
        global_heap->erase(es);
    }else{
        add_row(k1); 
    }
    
    row_heap = (*row_map)[k1];
    
    if(has_score(k1,k2)){                       // case 1. UPDATE
        es = (*(*pair_map)[k1])[k2];            //
        row_heap->erase(es);                    // delete from row_max_map
        es->score = sc;                         // update scores
        row_heap->insert(es);                   // re-insert

    }else{                                      // case 2. ADD
        es = new ps_t( k1, k2, sc );
        (*(*pair_map)[k1])[k2] = es;            // add into a pair map
        (*row_map)[k1]->insert(es);             // add into a row map
        num_pairs++;
    } 
    
    // add back this row's max into global max heap  
    // assert( row_heap->size() > 0);
    es = get_row_max(k1);
    global_heap->insert(es);
}

//
// Note: this also deletes edge_score from memory
template<typename key_t, typename value_t>
void score_matrix_t<key_t,value_t>::del_score( key_t k1, key_t k2 ){
#ifdef DEBUG
    assert( has_score(k1,k2) );
#endif
    SORT_PAIR(k1,k2);
    
    ps_map_t* row = get_row(k1);
    ps_t* es = (*row)[k2];  
    score_heap_t* row_heap = (*row_map)[k1];
    
    global_heap->erase(get_max_heap(row_heap)); // erase this row's max 
    row->erase(k2);                             // from pair_map(row, col) 
    row_heap->erase(es);                        // from row_map  
    if (row_heap->size() > 0){                  // re-insert this row's max
        global_heap->insert(get_max_heap(row_heap));
    }
    delete es;                                  // clean-up memory
    if (row->size()==0) del_row(k1);            // empty row
    num_pairs--;                                // decrease counter
}

// delete any scores involving key
template<typename key_t, typename value_t>
void score_matrix_t<key_t,value_t>::del_scores_with( key_t k ){

    // search any row containing k
    // delete (x,k) s.t. x < k
    ps_map_map_it it = pair_map->begin();
    ps_map_t* tmp_map;
    key_t x;
    for( ; it!= pair_map->end(); ){
        x = it->first; tmp_map = it->second; ++it;
        if( x < k && tmp_map->count(k) ) del_score(x,k);
    }
    if( has_row(k) ) del_row(k); 
}


template<typename key_t, typename value_t>
typename score_matrix_t<key_t,value_t>::ps_t* 
score_matrix_t<key_t,value_t>::get_score( key_t k1, key_t k2 ){
    SORT_PAIR(k1,k2);
#ifdef DEBUG
    assert( has_score(k1,k2) );
#endif
    return (*(*pair_map)[k1])[k2];
}


template<typename key_t, typename value_t>
typename score_matrix_t<key_t,value_t>::ps_t* 
score_matrix_t<key_t,value_t>::find_max_score(){
#ifdef DEBUG 
    assert( global_heap->size() > 0 );
#endif
    ps_t* es = get_max_heap(global_heap);
    return es;
}


//
// misc functions
//
template<typename key_t, typename value_t>
unsigned long score_matrix_t<key_t,value_t>::size(){
    return num_pairs;
}

template<typename key_t, typename value_t>
void score_matrix_t<key_t,value_t>::empty_mat(){
    
    // clean up pair_map and row_map
    ps_map_map_it it = pair_map->begin();
    key_t k;
    for ( ; it != pair_map->end(); ) {
        k = it->first;
        ++it;
        del_row( k );
    }
    // for (key_t k = min_id; k <= max_id; k++){
    //   if (!has_row(k)) continue ; 
    //   del_row( k ); 
    // }
    pair_map->clear();    // clean up pairwise map
    row_map->clear();     // clean up row-max heap
    global_heap->clear(); // clean up global max heap
    num_pairs = 0;
}



#endif
